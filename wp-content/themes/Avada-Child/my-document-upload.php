<?php
/**
 * Template name: My Dcoument Upload
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); 
if (is_user_logged_in()){
?>

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script> -->
<div class="cus-advertisement">
<?php
$files = $_FILES['attachment'];
  //echo $fileCount = count($files["name"]);
		$upload_overrides = array( 'test_form' => false );
		$filespath = ''; 
		foreach ($files['name'] as $key => $value) {
		    if ($files['name'][$key]) {
		    $uploadedfile = array(
		            'name'     => $files['name'][$key],
		            'type'     => $files['type'][$key],
		            'tmp_name' => $files['tmp_name'][$key],
		            'error'    => $files['error'][$key],
		            'size'     => $files['size'][$key]
		        );

			$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );

			if ( $movefile && ! isset( $movefile['error'] ) ) {
			    //echo "File is valid, and was successfully uploaded.\n";
			   /// var_dump( $movefile );
				$filespath .= $movefile ['url']. '%%';
			} else {
			    /**
			     * Error generated by _wp_handle_upload()
			     * @see _wp_handle_upload() in wp-admin/includes/file.php
			     */
			    //echo $movefile['error'];
			}

		}
	}
$filespath = $_POST['key_client_name_hidden'].$filespath;
//print_r($filespath);
?>

<h2>Add new Document</h2>
<?php if(is_user_logged_in()){ 
if($_POST){
	  global $wpdb;
	  $user = wp_get_current_user();
		$userid = $user->ID;
		$document_name=$_POST['document_name'];
		$document_description=$_POST['document_description'];
		/*$industry=$_POST['industry'];
		$contact=$_POST['contact'];
		$licenses=$_POST['licenses'];
		$country=$_POST['country'];
		$address=$_POST['address'];
		$certificates=$_POST['certificates'];
		$price=$_POST['price'];
		$year_establish=$_POST['year_establish'];
		$financing=$_POST['financing'];
		$legal_form=$_POST['legal_form'];
		$time_of_sale=$_POST['time_of_sale'];
		$employees=$_POST['employees'];
		$property=$_POST['property'];
		$turnover=$_POST['turnover'];
		$management=$_POST['management'];
		$description=$_POST['description'];*/
		$table_name = $wpdb->prefix . "upload_document";
		
		$query = $wpdb->insert($table_name, array('userid' => $userid ,'document_name' => $document_name,'upload_document'=>$filespath,'document_description' =>$document_description));
		
		if($query)
		{
		$msg="Dcoument uploaded successfully";
		echo '<h5 class="ad_success_msg">' . $msg.'</h5>';
		}
		else{
		echo 'There is some prolem found. Please try again later.';
	    }
	}
?>
		<form action="http://112.196.9.211:8888/companymarekt.ch/my-document-upload/" method="post" enctype="multipart/form-data"  data-toggle="validator" role="form" id="addadvertise">
			<div class="col-md-6">
				<div class="form-group">
					<label for="document_name" class="control-label">Document Name:</label>
					<input type="text" class="form-control" name="document_name" id="document_name" data-error="Please enter document name filed." required>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group upload-image">
					<label for="uploadname" class="control-label">Upload Document:</label>
					<input type="hidden" name="key_client_name_hidden" id="key_client_name" value="" class="regular-text key_client_name">
					<div class="input_key_client_name_wrap">
						<div><input type="file" name="attachment[]" class="regular-text" required></div>
					</div>
					<div class="add-attachment">
						<label for="key_client_name">Add Attachment</label>
						<button class="add_key_client_name_button">Add More </button>
					</div>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label for="document_description" class="control-label">Description:</label>
					<textarea class="form-control" name="document_description" id="document_description"></textarea>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			
			<!-- <div class="col-md-12">
				<div class="pay-now-btn text-center">
					<div class="amount_pay">Amount Paid for this advertisement:<span class="amount_pay_inner"></span></div>
					<a href="#" class="btn btn-lg btn-success" data-toggle="modal" data-target="#basicModal" id="buyy">Pay Now</a>
				</div>
			</div> -->
	<!-- 		<div class="modal fade pop-pay-now" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title" id="myModalLabel">Pay Now</h4>
						</div>
						<div class="modal-body">
							<div class="col-md-12">
								<div class="form-group">
									<label>Amount Paid is :</label>
									<input type="text" name="finalprice" id="finalprice" class="form-control" readonly>
									<div class="help-block with-errors"></div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary form-control">Save changes</button>
						</div>
					</div>
				</div>
			</div> -->
			<div class="cus-btn-submit">
				<div class="form-group">
					<!-- <input type="submit" name="submit_btn" class="btn btn-default form-control"> -->
					<button type="submit" class="btn btn-primary form-control">Save changes</button>
				</div>
			</div>
		</form>
	</div>
<?php
}
else{
echo '<h2 class="loggedout_msg">You have to logged in to view this page</h2>';
}
?>
	<script>
		jQuery(document).ready(function(){
   
			jQuery('a.delete').on('click',function(e){
				e.preventDefault();
				
				var repurl = jQuery(this).attr("href");				
				var replit = jQuery('input[name=key_client_name_hidden]').val().replace(repurl+'%%',"");
				jQuery('input[name=key_client_name_hidden]').val(replit);	
				imageID = jQuery(this).closest('.image1')[0].id;
				jQuery(this).closest('.image1')
					.fadeTo(300,0,function(){
						jQuery(this)
							.animate({width:0},200,function(){
								jQuery(this)
									.remove();
							});
					});
			});
			var max_fields      = 10; //maximum input boxes allowed
			var wrapper         = jQuery(".input_key_client_name_wrap"); //Fields wrapper
			var add_button      = jQuery(".add_key_client_name_button"); //Add button ID
			
			var x = 1; //initlal text box count
			jQuery(add_button).click(function(e){ //on add input button click
				e.preventDefault();
				if(x < max_fields){ //max input box allowed
					x++; //text box increment
					jQuery(wrapper).append('<div class="logo-add"> <input type="file" class="form-control" id="attachment" name="attachment[]" data-error="Please add attachment field" required><a href="#" class="remove_key_client_name" style="color:red;"><i class="fa fa-trash" aria-hidden="true"></i></a></div>'); //add input box
				}
			});
			
			jQuery(wrapper).on("click",".remove_key_client_name", function(e){ 
				e.preventDefault(); jQuery(this).parent('div').remove(); x--;
			})

			
			jQuery('#selling_price').change(function(){
				jQuery(".amount_pay_inner").empty();
				var salprice=jQuery('#selling_price').val();
				if(salprice>0 && salprice<=259000){
					var price = "550";
					jQuery(".amount_pay_inner").append(price);
					document.getElementById("finalprice").value = price;
				}
				else if(salprice>259000){
					var price = "200";
					jQuery(".amount_pay_inner").append(price);
					document.getElementById("finalprice").value = price;
				}
			})
		});
	</script>
<?php do_action( 'avada_after_content' ); 
}
else
{
echo '<h3 class="logged_msg">You are not authorized to view the content on this page.</h3>';
}
get_footer();
?>