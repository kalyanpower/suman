<?php 
global $wpdb;
$args = array( 'post_type' => 'testimonial', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
?>
<!---- testimonial slider --->
    <div class="section section-project">

            <h2 class="section__title"><b>TESTIMONIALS</b></h2>

            <div class="project-carousel">
                <div class="project-strip">
					<?php while ( $loop->have_posts() ) : $loop->the_post();
					$id=get_the_ID();
					$value = get_field("image");
					$designation = get_field("designation");
					$facebook=get_field("facebook_link");
					$linedin=get_field("linkedin");
					$twitter=$linedin=get_field("twitter");
					?>
                    <div class="project">
                        <div class="project">
                            <div class="project-back">
                                <div class="col-md-5 no-padding">
                                 <img class="person-img" src="<?php echo $value['url']; ?>" alt=""/>
                                </div>
                                <div class="col-md-7 no-padding">
                                    <h3><?php the_title();?></h3>
                                    <h5><?php echo $designation;?></h5>
                                    <p>
									<?php  echo '<div class="entry-content">';
									the_content();
									echo '</div>'; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
            </div>
            
               <div class="project-screen">
					<div class="project-detail">
						<?php while ( $loop->have_posts() ) : $loop->the_post();
						$value = get_field( "image" );
						$designation = get_field( "designation" );
						$facebook=get_field("facebook_link");
						$linedin=get_field("linkedin");
						$twitter=$linedin=get_field("twitter");
						?>
                        <div class="project">
                            <div class="project-front">
                                <div class="row no-margin">
								<img class="person-img" src="<?php echo $value['url'];;?>" alt=""/>
                                <ul> 
                                <li><a href="<?php echo $facebook;?>"><img src="<?php echo get_site_url();?>/wp-content/uploads/2018/03/facebook.png"></a></li>
								<li><a href="<?php echo $linedin;?>"><img src="<?php echo get_site_url();?>/wp-content/uploads/2018/03/twitter.png"></a></l1>
								<li><a href="<?php echo $twitter;?>"><img src="<?php echo get_site_url();?>/wp-content/uploads/2018/03/insta.png"></a></l1>
								</ul>
                                </div>
                                <div class="row no-margin">
                                    <blockquote>
                                    <div>
                                    <h3><?php the_title();?></h3>
                                    <h5><?php echo $designation;?></h5>
                                    <p>
									<?php  echo '<div class="entry-content">';
									the_content();
									echo '</div>';?>
                                    </p> 
									</div>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
						<?php endwhile; ?>
                        </div>
                        </div>
						<div class="screen-frame"></div>
                 </div>
					
            </div>
            <!---- testimonial slider end --->
        
