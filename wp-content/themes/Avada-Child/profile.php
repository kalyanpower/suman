<?php
/**
 * Template name: Profile Page
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<style>
.show_more{display:none;}
</style>

<?php get_header(); 
if (is_user_logged_in()){
	
global $wpdb;
$id = get_current_user_id();
$table_name = $wpdb->prefix . "advertisement"; 
$query= $wpdb->get_results("SELECT * from $table_name where userid ='$id'");

$queryy= $wpdb->get_results("SELECT * from $table_name where userid ='$id' ORDER BY id DESC
LIMIT 3");
	
?>
<section id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
	<?php while(have_posts()) : ?>
		<?php the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="post-content">
				<?php the_content(); ?>
				<?php fusion_link_pages(); ?>
			</div>
		</div>
	<?php endwhile; ?>
    <!-- What to sell you business -->
		<div class="what_business">
			<h1>Want to sell your business?</h1>
			<a href="#">create ad now!</a>
		</div>
	<!-- What to sell you business  end-->
	<!-- Slider section -->
		<section class="instagram">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="slider_test">
						<div class="slider4">
							
							<?php
							 foreach ($query as $queryvalue) {
              $img_src = array_filter(explode("%%",$queryvalue->upload_image));  
			  ?>
							<div class="slide">
								<a href="">
									<div class="img-blk">
										 <img class="group list-group-image item-image" src="<?php echo $img_src[0]; ?>" alt="featured image1" />
									</div>
									<div class="txt-blk">
										<div class="slider_txt">
											<h3><?php echo $queryvalue->description; ?></h3>
											<p>CHF <?php echo $queryvalue->selling_price; ?>M</p>
										</div>
									</div>
								</a>
							</div>
							 <?php } ?>
							
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>		
	<!-- slider end here -->
<!-- Form step setion start here -->
		<div class="step_section">
			<div class="container">
				<div class="step_txt">7 steps to success</div>
				<div class="stepwizard">
				    <div class="stepwizard-row setup-panel">
				        <div class="stepwizard-step">
				        	<p>Decide</p>
				            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
				        </div>
				        <div class="stepwizard-step">
				            <p>Prepare</p>
				            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
				        </div>
				        <div class="stepwizard-step">
				        	<p>Search</p>
				            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
				        </div>
						<div class="stepwizard-step">
							<p>Due Dillgence</p>
				            <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
				        </div>
						<div class="stepwizard-step">
							<p>Define</p>
				            <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
					    </div>
						<div class="stepwizard-step">
							<p>Contract</p>
				            <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
				        </div>
				        <div class="stepwizard-step">
							<p>Success!</p>
				            <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">67</a>
				        </div>
				    </div>	
				</div>
				<form role="form">
				    <div class="row setup-content" id="step-1">
				        <div class="col-xs-12">
				            <div class="col-md-12">
				               <div class="Step_txt">
				               		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five.</p>
				               		<div class="step_btb">
				               			<a href="#">got it</a>
				               			<a href="#">find out more</a>
				               		</div>
				               </div>
				            </div>
				        </div>
				    </div>
				    <div class="row setup-content" id="step-2">
				        <div class="col-xs-12">
				            <div class="col-md-12">
				                <div class="Step_txt">
				               		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five.</p>
				               		<div class="step_btb">
				               			<a href="#">got it</a>
				               			<a href="#">find out more</a>
				               		</div>
				               </div>
				            </div>
				        </div>
				    </div>
				    <div class="row setup-content" id="step-3">
				        <div class="col-xs-12">
				            <div class="col-md-12">
				                <div class="Step_txt">
				               		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown ok a galley of type and scrambled it to make a type specimen book. It has survived not only five.</p>
				               		<div class="step_btb">
				               			<a href="#">got it</a>
				               			<a href="#">find out more</a>
				               		</div>
				               </div>
				            </div>
				        </div>
				    </div>
				    <div class="row setup-content" id="step-4">
				        <div class="col-xs-12">
				            <div class="col-md-12">
				                <div class="Step_txt">
				               		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum hasthe industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five.</p>
				               		<div class="step_btb">
				               			<a href="#">got it</a>
				               			<a href="#">find out more</a>
				               		</div>
				               </div>
				            </div>
				        </div>
				    </div>
				    <div class="row setup-content" id="step-5">
				        <div class="col-xs-12">
				            <div class="col-md-12">
				                <div class="Step_txt">
				               		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has beendard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five.</p>
				               		<div class="step_btb">
				               			<a href="#">got it</a>
				               			<a href="#">find out more</a>
				               		</div>
				               </div>
				            </div>
				        </div>
				    </div>
				    <div class="row setup-content" id="step-6">
				        <div class="col-xs-12">
				            <div class="col-md-12">
				                <div class="Step_txt">
				               		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five.</p>
				               		<div class="step_btb">
				               			<a href="#">got it</a>
				               			<a href="#">find out more</a>
				               		</div>
				               </div>
				            </div>
				        </div>
				    </div>
				    <div class="row setup-content" id="step-7">
				        <div class="col-xs-12">
				            <div class="col-md-12">
				                <div class="Step_txt">
				               		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.</p>
				               		<div class="step_btb">
				               			<a href="#">got it</a>
				               			<a href="#">find out more</a>
				               		</div>
				               </div>
				            </div>
				        </div>
				    </div>
				</form>	
			</div>
		</div>
		


	<!-- Form step setion end here -->
	<!-- featured  Area Start -->
		  <div class="featured-section section-padding bg-color-grey">
			<div class="col-md-12 text-center section-heading a_container ">
				 <div class="container  a_header">
			 
					<h2 class="text-center">FEATURED ADS</h2>
					<div class="btn-group pull-right">
                    <a href="#" id="list"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/list-view-selected/list-view-selected.png" srcset="images/list-view-selected/list-view-selected@2x.png 2x,images/list-view-selected/list-view-selected@3x.png 3x" class="list_view-selected"> </a>
                    <a href="#" id="grid"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/grid-view/grid-view.png" srcset="images/grid-view/grid-view@2x.png 2x,images/grid-view/grid-view@3x.png 3x" class="grid_view"></a>
                </div>
					 
				</div>
			</div>
			<div class="section-content">
				<div class="container">
             
            <div id="products" class="row list-group ">
			
			<?php 
			foreach($query as $get_results){
				
				$img_img = array_filter(explode("%%",$get_results->upload_image));  

			?>
                <div class="show-more show_more" >
                <div class="itemm col-xs-12 col-lg-12 list-group-item">
                    <div class="thumbnail">
                  <img class="group list-group-image item-image" src="<?php echo $img_img[0]; ?>" alt="featured image1" />
                        
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/add-to-favorites/add-to-favorites.png" srcset="images/add-to-favorites/add-to-favorites@2x.png 2x,images/add-to-favorites/add-to-favorites@3x.png 3x" class="add_to_favorites">
                        <div class="caption">
                            <div class="row">
                                <div class="grid-fix col-md-12">
                                    <a href="#"><h4 class="group inner list-group-item-heading"><?php echo $get_results->property_name; ?></h4></a>
                                    <p class="group inner list-group-item-text"><?php echo $get_results->selling_price; ?> CHF</p>
                                </div>
                                </div>
                                <div class="grid-fix col-xs-12 col-md-12 btn-pos">
                                    <a class="btn a-btn cat-hov" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/category/category.png" srcset="images/category/category@2x.png 2x,images/category/category@3x.png 3x" class="category-hover"><?php echo $get_results->industry; ?></a>
                                    <a class="btn a-btn loc-hov" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/location/location.png" srcset="images/location/location@2x.png 2x,images/location/location@3x.png 3x" class="location-hover"><?php echo $get_results->country; ?></a>
                                </div>
                                <div class="grid-fix col-xs-12 col-md-12 item-footer-status">
                                    <p><strong>Employees</strong> <?php echo $get_results->employees; ?> </p>
                                    <p><strong>Turnover</strong> <?php echo $get_results->turnover; ?></p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
				<?php  } ?>
				
				
                </div>
            </div>
                <div class="a_footer">
                    <p class="text-center"><a id="nextt" href="#"><strong>SHOW MORE</strong></a> </p>
                </div>
            </div>
			</div>	
		</div>  
	<!-- featured  Area Ends -->
	
	<!-- Recommended ads start -->
		<div class="recommended_section section-padding ">
			<div class="recommended_txt">
				<h1>Recommended Ads</h1>
			</div>
			<div id="products" class="row list-group ">
	                <div class="show-more">
					<?php
					foreach($queryy as $get_data_result){
				
				$img_img = array_filter(explode("%%",$get_data_result->upload_image));  

			?>
					
	                <div class="item  col-xs-12 col-lg-12 grid-group-item" style="display: flex;">
	                    <div class="thumbnail">
	                        <img class="group list-group-image item-image" src="<?php echo $img_img[0]; ?>" alt="featured image1">
	                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/add-to-favorites/add-to-favorites.png" srcset="<?php echo get_stylesheet_directory_uri(); ?>/images/add-to-favorites/add-to-favorites@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/add-to-favorites/add-to-favorites@3x.png 3x" class="add_to_favorites">
	                        <div class="caption">
	                            <div class="row">
	                                <div class="grid-fix col-md-12">
	                                    <a href="#"><h4 class="group inner list-group-item-heading"><?php echo $get_data_result->property_name; ?></h4></a>
	                                    <p class="group inner list-group-item-text"><?php echo $get_data_result->selling_price; ?> CHF</p>
	                                </div>
	                                <div class="grid-fix col-xs-12 col-md-12 btn-pos">
	                                    <a class="btn a-btn cat-hov" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/category/category.png" srcset="<?php echo get_stylesheet_directory_uri(); ?>/images/category/category@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/category/category@3x.png 3x" class="category-hover"><?php echo $get_data_result->industry; ?></a>
	                                    <a class="btn a-btn loc-hov" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/location/location.png" srcset="<?php echo get_stylesheet_directory_uri(); ?>/images/location/location@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/location/location@3x.png 3x" class="location-hover"><?php echo $get_data_result->country; ?></a>
	                                </div>
	                                <div class="grid-fix col-xs-12 col-md-12 item-footer-status">
	                                    <p><strong>Employees</strong><?php echo $get_data_result->employees; ?> </p>
	                                    <p><strong>Turnover</strong><?php echo $get_data_result->turnover; ?></p>
	                                </div>

	                            </div>
	                        </div>
	                    </div>
	                </div>
					<?php  } ?>
					
					
	                </div>
	            </div>
            </div>
	<!-- Recommended ads end -->
	<!---------------------slider logo start -------------------------->
    <div class="row text-center rel-pos">
        <button id="sd-button" class="btn hide-btn">^</button>
        <div class="sd-container">
            <div class="section__title">
                <h2 class="section__title"><b>SUCCESSFUL DEALS</b></h2>
            </div>
            <div class="sd-slider">
                <div><img id="foursquare" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo/foursquare.png"></div>
                <div><img id="swarm" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo/swarm.png"></div>
                <div><img id="vine" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo/vine.png"></div>
                <div><img id="jelly" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo/jelly.png"></div>
                <div><img id="beats" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo/beats.png"></div>
                <div><img id="skype" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo/skype.png"></div>
                <div><img id="bitbucket" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo/bitbucket.png"></div>
            </div>
        </div>
    </div>
<!--------------------slider logo end ---------------------------->

</section>
<script>
$(document).ready(function(){
	$(function(){
    $(".show_more").slice(0, 3).show(); // select the first ten
    $("#nextt").click(function(e){ // click event for load more
        e.preventDefault();
        $(".show_more:hidden").slice(0, 5).show(); // select next 10 hidden divs and show them
        if($(".show_more:hidden").length == 0){ // check if any hidden divs still exist
           // alert("No more divs"); // alert if there are none left
        }
    });
});
	
	
});


</script>



<?php do_action( 'avada_after_content' ); 
}
else
{
echo '<h3 class="logged_msg">You are not authorized to view the content on this page.</h3>';
}
get_footer();
?>