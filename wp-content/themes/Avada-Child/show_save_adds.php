<?php
/**
 * Template name: Show Save Ads
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php
get_header();
if(is_user_logged_in()){ 
global $wpdb;
$id = get_current_user_id();
$table_advertisement = $wpdb->prefix . "advertisement"; 
$table_like_status = $wpdb->prefix . "like_status"; 

//$query= $wpdb->get_results("SELECT * from $table_name where userid ='$id' ");
$query_like = $wpdb->get_results("SELECT * from wp_like_status where user_id ='$id' AND like_status ='1' ");


// SELECT * FROM $table_advertisement JOIN $table_like_status ON $table_advertisement.id=wp_like_status.ads_id WHERE $table_advertisement.id=$table_like_status.ads_id AND $table_like_status.like_status=1 And $table_like_status.user_id=$id

$query= $wpdb->get_results("SELECT * FROM $table_advertisement JOIN $table_like_status ON $table_advertisement.userid=$table_like_status.user_id WHERE $table_advertisement.id=$table_like_status.ads_id AND $table_like_status.like_status=1 And $table_like_status.user_id=$id");

foreach ($query_like as $key => $query_like_value) {

   $query = $wpdb->get_results("SELECT * from $table_advertisement where id ='$query_like_value->ads_id'");

  // print_r($query);

?>
<div class="double-grid">
<div class="row">
			<div class="col-md-8">
            <?php 
            if($query){
            foreach ($query as $queryvalue) {
              $img_src = array_filter(explode("%%",$queryvalue->upload_image));  
              ?> 
                <div class="col-md-6 double_middle">
                    <div class="thumbnail">
                        <img class="group list-group-image item-image" src="<?php echo $img_src[0]; ?>" alt="featured image1" />
                        <img src="<?php echo ADS_URL; ?>/images/add-to-favorites/add-to-favorites.png" srcset="<?php echo ADS_URL; ?>/images/add-to-favorites/add-to-favorites@2x.png 2x,<?php echo ADS_URL; ?>/images/add-to-favorites/add-to-favorites@3x.png 3x" class="add_to_favorites">
                        <div class="caption">
                                <div class="txt_item">
                                    <a href="#"><h4 class="group inner list-group-item-heading"><?php echo '<a href="'. site_url() .'/property/?ad=' . $queryvalue->property_name . '">' . $queryvalue->property_name . '</a>'; ?></h4></a>
                                    <p class="group inner list-group-item-text"><?php echo $queryvalue->selling_price; ?> CHF</p>
                                </div>
                                <div class="grid-fix btn-pos">
                                    <a class="btn a-btn cat-hov" href="#"><img src="<?php echo ADS_URL; ?>/images/category/category.png" srcset="<?php echo ADS_URL; ?>/images/category/category@2x.png 2x,<?php echo ADS_URL; ?>/images/category/category@3x.png 3x" class="category-hover"><?php echo $queryvalue->industry; ?></a>
                                    <a class="btn a-btn loc-hov" href="#"><img src="<?php echo ADS_URL; ?>/images/location/location.png" srcset="<?php echo ADS_URL; ?>/images/location/location@2x.png 2x,<?php echo ADS_URL; ?>/images/location/location@3x.png 3x" class="location-hover"><?php echo $queryvalue->country; ?></a>
                                </div>
                                <div class="grid-fix col-xs-12 col-md-12 item-footer-status">
                                    <p><strong>Employees</strong><?php echo $queryvalue->employees; ?></p>
                                    <p><strong>Turnover</strong><?php echo $queryvalue->turnover; ?></p>
                                </div>

                           
                        </div>
                    </div>
            </div>
            <?php } } ?>
               </div>
              <div class="col-md-4"></div>
            </div>
            </div>
<?php }
}
else{
echo '<h2 class="loggedout_msg">You have to logged in to view this page</h2>';
}
?>
<?php do_action( 'avada_after_content' ); 

get_footer();
?>