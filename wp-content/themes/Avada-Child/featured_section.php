<!-- featured  Area Start -->
<?php
global $wpdb;
$table = $wpdb->prefix . "advertisement";
$id = get_current_user_id();
$query = $wpdb->get_results("SELECT * from $table where userid = '$id'");

?>
<style>
.show_more{display: none;}
</style>

		  <div class="featured-section section-padding bg-color-grey">
			<div class="col-md-12 text-center section-heading a_container ">
				 <div class="container  a_header">
			 
					<h2 class="text-center">FEATURED ADS</h2>
					<div class="btn-group pull-right">
                    <a href="#" id="list"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/list-view-selected/list-view-selected.png" srcset="<?php echo get_stylesheet_directory_uri(); ?>/images/list-view-selected/list-view-selected@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/list-view-selected/list-view-selected@3x.png 3x" class="list_view-selected"> </a>
                    <a href="#" id="grid"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/grid-view/grid-view.png" srcset="<?php echo get_stylesheet_directory_uri(); ?>/images/grid-view/grid-view@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/grid-view/grid-view@3x.png 3x" class="grid_view"></a>
                    </div>
					 
				</div>
			</div>
			<div class="section-content">
				<div class="container">
             
            <div id="products" class="row list-group ">
                <div class="show-more">
				
				<?php foreach($query as $get_data){ 
				
				$imagee = array_filter(explode('%%',$get_data->upload_image));
				?>
				 <div class="show_more">
                <div class="items  col-xs-12 col-lg-12 list-group-item">
                    <div class="thumbnail">
                        <img class="group list-group-image item-image" src="<?php echo $imagee[0]; ?>" alt="featured image1" />
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/add-to-favorites/add-to-favorites.png" srcset="<?php echo get_stylesheet_directory_uri(); ?>/images/add-to-favorites/add-to-favorites@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/add-to-favorites/add-to-favorites@3x.png 3x" class="add_to_favorites">
                        <div class="caption">
                            <div class="row">
                                <div class="grid-fix col-md-12">
                                    
                                 <a href="<?php echo site_url() .'/property/?ad=' . $get_data->property_name; ?>"><h4 class="group inner list-group-item-heading"><?php echo $get_data->property_name; ?></h4></a>
                                 <p class="group inner list-group-item-text"><?php echo $get_data->selling_price; ?> CHF</p>
                                </div>
                                <div class="grid-fix col-xs-12 col-md-12 btn-pos">
                                    <a class="btn a-btn cat-hov" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/category/category.png" srcset="<?php echo get_stylesheet_directory_uri(); ?>/images/category/category@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/category/category@3x.png 3x" class="category-hover"><?php echo $get_data->industry; ?></a>
                                    <a class="btn a-btn loc-hov" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/location/location.png" srcset="<?php echo get_stylesheet_directory_uri(); ?>/images/location/location@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/location/location@3x.png 3x" class="location-hover"><?php echo $get_data->country; ?></a>
                                </div>
                                <div class="grid-fix col-xs-12 col-md-12 item-footer-status">
                                    <p><strong>Employees</strong><?php echo $get_data->employees; ?></p>
                                    <p><strong>Turnover</strong>  <?php echo $get_data->turnover; ?></p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
				 </div>
				<?php } ?>
			
                </div>
            </div>
                <div class="a_footer">
                 <p class="text-center"><a id="nexttt" href="#"><strong>SHOW MORE</strong></a> </p>
                </div>
            </div>
			</div>	
			
		</div>  
		
		<script>

$(document).ready(function(){
	$(function(){
    $(".show_more").slice(0, 3).show(); // select the first ten
    $("#nexttt").click(function(e){ // click event for load more
        e.preventDefault();
        $(".show_more:hidden").slice(0, 5).show(); // select next 10 hidden divs and show them
        if($(".show_more:hidden").length == 0){ // check if any hidden divs still exist
            alert("No more divs"); // alert if there are none left
        }
    });
});
});

</script>
		
	<!-- featured  Area Ends -->