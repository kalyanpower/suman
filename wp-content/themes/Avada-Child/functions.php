<?php

function theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ) );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' );

add_shortcode( 'homeslide', 'wpdocs_footag_func' );

function wpdocs_footag_func( $atts ){
	require_once( __DIR__ . '/home.php');
}
add_shortcode( 'featured_ads', 'wpdocs_featured_func' );

function wpdocs_featured_func( $atts ){
	require_once( __DIR__ . '/featured_section.php');
}
add_shortcode( 'testimonials', 'wpdocs_testimonial_func' );

function wpdocs_testimonial_func( $atts ){
	require_once( __DIR__ . '/testimonials.php');
}
function create_posttype(){
 
    register_post_type( 'testimonial',
    
        array(
            'labels' => array(
                'name' => __( 'Testimonial' ),
                'singular_name' => __( 'Testimonial' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'testimonial'),
        )
    );
}
add_action( 'init', 'create_posttype' );

function tml_user_register($user_id){
	if ( !empty( $_POST['first_name'] ) )
		update_user_meta( $user_id, 'first_name', $_POST['first_name'] );
	if ( !empty( $_POST['last_name'] ) )
		update_user_meta( $user_id, 'last_name', $_POST['last_name'] );
	if ( !empty( $_POST['company_name'] ) )
		update_user_meta( $user_id, 'company_name', $_POST['company_name'] );
	if ( !empty( $_POST['phone'] ) )
		update_user_meta( $user_id, 'phone', $_POST['phone'] );
	if ( !empty( $_POST['address'] ) )
		update_user_meta( $user_id, 'address', $_POST['address'] );
	if ( !empty( $_POST['zip_code'] ) )
		update_user_meta( $user_id, 'zip_code', $_POST['zip_code'] );
	if ( !empty( $_POST['city'] ) )
		update_user_meta( $user_id, 'city', $_POST['city'] );
	if ( !empty( $_POST['country'] ) )
		update_user_meta( $user_id, 'country', $_POST['country'] );
	if ( !empty( $_POST['mem_levels'] ) )
	update_user_meta( $user_id, 'mem_levels', $_POST['mem_levels'] );
	wp_set_current_user($user_id);
    wp_set_auth_cookie($user_id);
    // You can change home_url() to the specific URL,such as 
    wp_redirect( home_url() . '/index.php/payment');
    //wp_redirect( home_url() );
    exit;
}
add_action( 'user_register', 'tml_user_register' );

function yoursite_extra_user_profile_fields($user_id){
	 $pass1 = $_POST['pass1'];
	 $pass2 = $_POST['pass2'];
	
		
	if ( !empty( $_POST['first_name'] ) )
		update_user_meta( $user_id, 'first_name', $_POST['first_name'] );
	if ( !empty( $_POST['last_name'] ) )
		update_user_meta( $user_id, 'last_name', $_POST['last_name'] );
	if ( !empty( $_POST['company_name'] ) )
		update_user_meta( $user_id, 'company_name', $_POST['company_name'] );
	if ( !empty( $_POST['phone'] ) )
		update_user_meta( $user_id, 'phone', $_POST['phone'] );
	if ( !empty( $_POST['address'] ) )
		update_user_meta( $user_id, 'address', $_POST['address'] );
	if ( !empty( $_POST['zip_code'] ) )
		update_user_meta( $user_id, 'zip_code', $_POST['zip_code'] );
	if ( !empty( $_POST['city'] ) )
		update_user_meta( $user_id, 'city', $_POST['city'] );
	if ( !empty( $_POST['country'] ) )
		update_user_meta( $user_id, 'country', $_POST['country'] );
	if ( !empty( $_POST['mem_levels'] ) )
	update_user_meta( $user_id, 'mem_levels', $_POST['mem_levels'] );
	
	
	
	if($pass1 == $pass2){
		if ( !empty( $_POST['pass1'] ) )
		update_user_meta( $user_id, 'password', $_POST['pass1'] );
	if ( !empty( $_POST['pass2'] ) )
		update_user_meta( $user_id, 'confirm_password', $_POST['pass2'] );
	}else{
		
		//echo "Please Enter Match Password";
		return false;
	}
	
    /*
	if ( !empty( $_POST['pass1'] ) )
		update_user_meta( $user_id, 'password', $_POST['pass1'] );
	if ( !empty( $_POST['pass2'] ) )
		update_user_meta( $user_id, 'confirm_password', $_POST['pass2'] );
    */
	
	
	
	
	
	
	wp_set_current_user($user_id);
    wp_set_auth_cookie($user_id);
    // You can change home_url() to the specific URL,such as 
    wp_redirect( home_url() . '/edit_profile/');
    //wp_redirect( home_url() );
    exit;
}
add_action( 'personal_options_update', 'yoursite_extra_user_profile_fields' );


add_filter('upload_mimes','add_custom_mime_types');
function add_custom_mime_types($mimes) {
    return array_merge($mimes,array (
            'pdf'                          => 'application/pdf',
            'doc'                          => 'application/msword',
            'pot|pps|ppt'                  => 'application/vnd.ms-powerpoint',
            'wri'                          => 'application/vnd.ms-write',
            'xla|xls|xlt|xlw'              => 'application/vnd.ms-excel',
            'mdb'                          => 'application/vnd.ms-access',
            'mpp'                          => 'application/vnd.ms-project',
            'docx'                         => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'docm'                         => 'application/vnd.ms-word.document.macroEnabled.12',
            'dotx'                         => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
            'dotm'                         => 'application/vnd.ms-word.template.macroEnabled.12',
            'xlsx'                         => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'xlsm'                         => 'application/vnd.ms-excel.sheet.macroEnabled.12',
            'xlsb'                         => 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
            'xltx'                         => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
            'xltm'                         => 'application/vnd.ms-excel.template.macroEnabled.12',
            'xlam'                         => 'application/vnd.ms-excel.addin.macroEnabled.12',
            'pptx'                         => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'pptm'                         => 'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
            'ppsx'                         => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
            'ppsm'                         => 'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
            'potx'                         => 'application/vnd.openxmlformats-officedocument.presentationml.template',
            'potm'                         => 'application/vnd.ms-powerpoint.template.macroEnabled.12',
            'ppam'                         => 'application/vnd.ms-powerpoint.addin.macroEnabled.12',
            'sldx'                         => 'application/vnd.openxmlformats-officedocument.presentationml.slide',
            'sldm'                         => 'application/vnd.ms-powerpoint.slide.macroEnabled.12',
            'onetoc|onetoc2|onetmp|onepkg' => 'application/onenote',
    ));
}

function update_save_adds(){
	global $wpdb;
	$like = $_POST['like'];
	
	$auto_id = $_POST['auto_id'];
	
	$user_idd = $_POST['user_idd'];

	//echo "INSERT INTO wp_like_status (ads_id, user_id, like_status) VALUES ('$auto_id', '$user_idd', '$like')";

  $queryy = $wpdb->get_results("SELECT * from 	wp_like_status WHERE user_id='$user_idd' AND ads_id='$auto_id'");
		foreach($queryy as $queryyy){	
		}

		 $rowcount = count($queryy);
		if($rowcount > 0){
			 $wpdb->query($wpdb->prepare("UPDATE wp_like_status SET ads_id = '$auto_id', user_id = '$user_idd', like_status = '$like' WHERE id = $queryyy->id"));
			} else {
		 $wpdb->query($wpdb->prepare("INSERT INTO wp_like_status (ads_id, user_id, like_status) VALUES ('$auto_id', '$user_idd', '$like')"));
		}
	
	}
	
add_action('wp_ajax_update_save_adds', 'update_save_adds');
add_action( 'wp_ajax_nopriv_update_save_adds', 'update_save_adds');

function delete_upload_docs(){
                global $wpdb;
                
                $docs_id = $_POST['docs_id'];
                
                $table_name = $wpdb->prefix . "upload_document"; 

                $query = $wpdb->query('DELETE  FROM '.$table_name.'  WHERE id = "'.$docs_id.'"');
}
add_action('wp_ajax_delete_upload_docs', 'delete_upload_docs');
add_action( 'wp_ajax_nopriv_delete_upload_docs', 'delete_upload_docs');

function insert_ads_message(){
    global $wpdb;
    
    $message = $_POST['message'];
    $userid = $_POST['userid'];
    $ads_id = $_POST['ads_id'];
    $reciver_id = $_POST['reciver_id'];
    
    $table_name = $wpdb->prefix . "ads_message"; 
    
  $query = $wpdb->query($wpdb->prepare("INSERT INTO $table_name (ads_id, userid, sender_id, receiver_id, message, read_status) VALUES ('$ads_id', '$userid', '$userid', '$reciver_id', '$message', '0')"));

  print_r($query);
  
  die();
}
add_action('insertAdsMessages', 'insert_ads_message');
add_action('wp_ajax_insertAdsMessages', 'insert_ads_message');
add_action( 'wp_ajax_nopriv_insertAdsMessages', 'insert_ads_message');

function get_ads_message(){
    global $wpdb;
   $userid = $_POST['userid']; 

   $intVal = $_POST['intVal']; 

    $table_name = $wpdb->prefix . "ads_message"; 

    //echo "SELECT * from $table_name WHERE userid='$userid'";

   $query = $wpdb->get_results("SELECT * from $table_name WHERE receiver_id='$userid' ORDER BY ads_msg_id DESC");

   $msg = '';
   $userid = get_current_user_id();
   $url =get_stylesheet_directory_uri();
 	$addClass ="";
   	foreach ($query as $queryval) { 

		$date1 = strtotime($queryval->message_created_at);
		$date2 = strtotime(date('Y-m-d H:i:s'));
		$seconds_diff = $date2 - $date1;

		//echo $agoTime = round(abs($seconds_diff) / 3600,2). " hours ago";
		/*if($seconds_diff>=60){
			$agoTime = round(abs($seconds_diff) / 60). " minute ago";
		
		}*/

		if($seconds_diff>=60){
			$agoTime = round(abs($seconds_diff) / 60). " minute ago";
		}
		if($seconds_diff>=3600){
			$agoTime = round(abs($seconds_diff) / 3600). " hours ago";
		}
		if($seconds_diff>=86400){
			$agoTime = round(abs($seconds_diff) / 86400). " Day ago";
		}
		if($seconds_diff>=2592000){
			$agoTime = round(abs($seconds_diff) / 86400). " month ago";
		}
		if($seconds_diff>=31104000){
			$agoTime = round(abs($seconds_diff) / 86400). " Year ago";
		}


   		$table_name2 = $wpdb->prefix . "users";
   		$table_name3 = $wpdb->prefix . "usermeta";

   		$query2 = $wpdb->get_results("SELECT * from $table_name2 JOIN $table_name3 ON ($table_name2.ID = $table_name3.user_id) where $table_name2.ID='$queryval->sender_id'");
   		/*echo"<pre>";
   		//print_r($query2[13]->meta_value);
   		print_r($query2);
   		echo"</pre>";*/
   		//echo"<pre>";
   		if($query2[0]->user_login=='admin'){
   			$company = $query2[48]->meta_value;
   		}else{
   			$company = $query2[13]->meta_value;
   		}

   		
   		
   		//print_r();
   		//echo"</pre>";

   		if($queryval->read_status==0){
   			$addClass ="unread";
   		}else{
   			$addClass ="";
   		}
   		            
            
        $msg .= '<div class="message '.$addClass.'">';
   	   	$msg .= '<div class="sender-info">';
   	   	$msg .= '<a class="msg_read_status" Onclick="MessageReadStatus('.$queryval->ads_msg_id.');" href="'.esc_url( add_query_arg( 'id', $queryval->ads_msg_id, get_permalink( 405 ) ) ).'">';
        $msg .= '<img src="http://placehold.it/34x34/000/fff" class="sender-pf-pic">';
        $msg .= '<div class="text-block">';
	    $msg .= '<h5 class="sender-name">'.$query2[0]->display_name.'</h5>';
	    $msg .= '<h6 class="sender-pro">'.$company.'</h6>';
        $msg .= '</div>';
        $msg .= '</a>';
        $msg .= '</div>';
        $msg .= '<div class="message-time">';
        $msg .= '<h6 class="time">'.$agoTime.'</h6>';
        $msg .= '<h6 class="message-count"></h6>';
        $msg .= '</div>';
        $msg .= '<div class="message-content">';
        $msg .= '<p class="message-prev">'.mb_strimwidth($queryval->message, 0, 150, "...").'</p>';
        $msg .= '<input type="hidden" name="ads_id" id="ads_id" class="ads_id" value="'.$queryval->ads_id.'" />';
        $msg .= '<input type="hidden" name="reciver_id" id="reciver_id" class="reciver_id" value="'.$queryval->sender_id.'" />';
        $msg .= '<button class="reply icon_reply" Onclick="setReplyMessage('.$queryval->ads_msg_id.');" data-toggle="modal" data-target="#Modal_rply"><img src="'.$url.'/images/reply-message/reply-message.png" srcset="image/reply-message/reply-message@2x.png 2x, image/reply-message/reply-message@3x.png 3x"></button>';
        $msg .= '<button class="delete icon_del" Onclick="ConfirmDelete('.$queryval->ads_msg_id.');"><img src="'.get_stylesheet_directory_uri().'/images/delete-message/delete-message.png" srcset="image/delete-message/delete-message@2x.png 2x, image/delete-message/delete-message@3x.png 3x"></button>';
       	$msg .= '</div>';  
       	$msg .= '</div>'; 
    }     
        
    echo json_encode($msg);
    //print_r($queryval->message);
 die();
}
add_action('getMessages', 'get_ads_message');
add_action('wp_ajax_getMessages', 'get_ads_message');
add_action( 'wp_ajax_nopriv_getMessages', 'get_ads_message');

function deleteMessage(){ ?>
<script type="text/javascript">	  
    function ConfirmDelete(msg_id){

        jQuery.ajax({
            url: "<?php echo admin_url('admin-ajax.php'); ?>",
            type: 'POST',
            data: { msg_id : msg_id,
            action: 'DeleteMessage'
            },
            beforeSend: function() {
        
       			 var x = confirm("Are you sure you want to delete?");
					if (x)
					return true;
					else
					return false;
   			 },
            success: function(response) {
                if(response==1){
                	//alert("Message was deleted successfully");
                }else{
                    //jQuery('.message').html(response);
                    alert("Message was not deleted");
                }
            }              
        });
    }

</script>
<?php 
}
add_action('wp_footer', 'deleteMessage');

function delete_ads_message(){
    global $wpdb;
    
    $msg_id = $_POST['msg_id'];    
    $table_name = $wpdb->prefix . "ads_message"; 
    $query = $wpdb->query('DELETE  FROM '.$table_name.'  WHERE ads_msg_id = "'.$msg_id.'"');
    print_r($query);
    die();
}
add_action('DeleteMessage', 'delete_ads_message');
add_action('wp_ajax_DeleteMessage', 'delete_ads_message');
add_action( 'wp_ajax_nopriv_DeleteMessage', 'delete_ads_message');

function ReplyMessage(){ ?>
<script type="text/javascript">	  

	function setReplyMessage(msgId){
		console.log(msgId);

		jQuery('#messgae-id').val(msgId);	
		//jQuery('#rply_name').val(msgId);
		var messgae = jQuery('textarea#rply_name').val();
		
	}


function MessageReadStatus(msgId){

	jQuery.ajax({
        url: "<?php echo admin_url('admin-ajax.php'); ?>",
        type: 'POST',
        data: { msg_id : msgId,
        action: 'UpdateMessageStatus'
        },
        success: function(response) {
            if(response==0){
            	//alert("Message was deleted successfully");
            }else{
                //jQuery('.message').html(response);
                alert("Message was not deleted");
            }
        }              
    });		
}
</script>
<?php 
}
add_action('wp_footer', 'ReplyMessage');

function reply_ads_message(){
    global $wpdb;
    
   $message = $_POST['message'];
   $userid = $_POST['userid'];
   $ads_id = $_POST['ads_id'];
   $reciver_id = $_POST['reciver_id'];
    
   $table_name = $wpdb->prefix . "ads_message"; 

   $query = $wpdb->query($wpdb->prepare("INSERT INTO $table_name (ads_id, userid, sender_id, receiver_id, message, read_status) VALUES ('$ads_id', '$userid', '$userid', '$reciver_id', '$message', '0')"));
  print_r($query);
  
  die();
}
add_action('MessageReply', 'reply_ads_message');
add_action('wp_ajax_MessageReply', 'reply_ads_message');
add_action( 'wp_ajax_nopriv_MessageReply', 'reply_ads_message');

/*function cancelRedirect(){ ?>
<script>
	function BackMessage(){
		window.location.href = "<?php echo site_url('/inbox-message');?>";
	}
	
</script>
<?php } 
add_action('wp_footer', 'cancelRedirect');*/

function ads_message_read_status_check(){
    global $wpdb;
    
   	$msg_id = $_POST['msg_id'];
   
    $table_name = $wpdb->prefix . "ads_message"; 
	$query = $wpdb->query($wpdb->prepare("UPDATE $table_name SET read_status='1' WHERE ads_msg_id=$msg_id"));
 	
	die();
}
add_action('UpdateMessageStatus', 'ads_message_read_status_check');
add_action('wp_ajax_UpdateMessageStatus', 'ads_message_read_status_check');
add_action( 'wp_ajax_nopriv_UpdateMessageStatus', 'ads_message_read_status_check');


function save_search(){
    global $wpdb;
   	$search = $_POST['seacrh'];
	$user_id = $_POST['user_id'];
    $table_name = $wpdb->prefix . "save_search_data"; 
	$query = $wpdb->get_results("Select * from $table_name WHERE user_id = '$user_id' AND save_search = '$search' ");
	echo $cc = count($query);
	foreach($query as $data_get){}
	echo "<pre>"; print_r($data_get->save_search); echo "</pre>";
	die;
	$query = $wpdb->query($wpdb->prepare("INSERT INTO $table_name (user_id, save_search)
VALUES ('$user_id','$search')"));
return $query;	
}
add_action('wp_ajax_save_search', 'save_search');


function pr($value)
    {
      echo "<pre>";
        print_r($value);
      echo "</pre>";
    }


/*function wpse_load_custom_search_template(){
    if( isset($_REQUEST['search']) == 'advanced' ) {
        require('search-page.php');
        //die();
    }
}
add_action('init','wpse_load_custom_search_template');*/

add_action('template_include', 'advanced_search_template');
function advanced_search_template( $template ) {
  if ( isset( $_REQUEST['search'] ) && $_REQUEST['search'] == 'advanced' && is_search() ) {
     $t = locate_template('search-page.php');
     if ( ! empty($t) ) {
         $template = $t;
     }
  }
  return $template;
}