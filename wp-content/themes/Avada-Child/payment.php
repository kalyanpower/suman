<?php
/**
 * Template name: Payment
 *
 * @package Avada
 * @subpackage Templates
 */
// Do not allow directly accessing this file.
if (! defined( 'ABSPATH')) {
	exit( 'Direct script access denied.' );
}
get_header(); 
if(is_user_logged_in()){
global $wpdb;
$user = wp_get_current_user();
$table_name = $wpdb->prefix . "membership_levels"; 
$query= $wpdb->get_results("SELECT * from $table_name");
?>
<section id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
	<?php while ( have_posts() ) : ?>
		<?php the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="post-content">
				<?php the_content(); ?>
				<form method="post" action="#">
				<?php $user_id = $user->ID; ?>
				<input type="hidden" name="userid" value="<?php echo $user_id; ?>">
				<?php $member_level = get_user_meta($user_id, 'mem_levels' , true ); ?>
			    <h3>Select the membership plan and Pay for this:</h3>
				<?php foreach($query as $q){ ?>
				<div class="payment">
				<input type="radio" name="mem_levels" value="<?php echo $q->id; ?>" <?php if(($member_level) == ($q->id)){ echo 'checked'; } ?> required>&nbsp;&nbsp;<?php echo $q->level_name; ?>
				</div>
				<?php } ?>
				<div class="payment_btn">
					<input type="submit" value="Submit" name="mem_submit">
				</div>
				</form>
			</div>
		</div>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
</section>
<?php
if(isset($_POST['mem_submit'])){
global $wpdb;
$userid=$_POST['userid'];
$table_name_n1 = $wpdb->prefix . "transaction_history";
$count = $wpdb->get_var("SELECT count(*) from $table_name_n1 where userid = '$userid'");
	if($count > 0){
	 echo '<p class="success_message">You already have subscribed to the package</p>';	
	}
	else{
	$mem_level=$_POST['mem_levels'];
	$table_name = $wpdb->prefix . "membership_levels"; 
	$table_name_n = $wpdb->prefix . "transaction_history"; 
	$query_mem = $wpdb->get_results("SELECT level_price from $table_name where id = '$mem_level'");
	$price = $query_mem[0]->level_price;
	$status = 1;
	update_user_meta($userid, 'mem_levels', $mem_level);
	$query=$wpdb->insert($table_name_n, array(
	'trans_id' => '',
	'userid' => $userid,
	'memberid' => $mem_level,
	'memberprice' => $price,
	'status' => $status
	));
	if($query)
	{
	echo '<p class="success_message">Payment Successful</p>';
	}
	}
}
}
else{ ?>
<section id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
	<?php while (have_posts()) : ?>
		<?php the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="post-content">
			<?php the_content(); ?>
			<?php echo '<h3>Please signup to buy the membership and make the payment</h3>'; ?>
			</div>
		</div>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
</section>
<?php
}
do_action('avada_after_content'); 
get_footer(); ?>
<script>
	$(document).ready(function(){
		$('input:checked').parent().addClass("active");
		$(".payment").click(function(){
			$(".payment").removeClass("active");
			$(this).addClass("active");
		});
	});

</script>