
//max characters to show in square offer description
$(function(){
    $(".sqare-offer h6").each(function(i){
        len=$(this).text().length;
        if(len>80)
        {
            $(this).text($(this).text().substr(0,115));
        }
    });
});

//square offer fill icons
$("#companyoffer").hover(
    function () {
    $("#companyoffer img").attr("src","image/company-fill/company-fill.png")
        .attr("srcset","image/company-fill/company-fill@2x.png 2x,image/company-fill/company-fill@3x.png 3x");
    },
    function () {
        $("#companyoffer img").attr("src","image/company/company-stroke.png")
            .attr("srcset","image/company/company-stroke@2x.png 2x,image/company/company-stroke@3x.png 3x");
    });
$("#expertoffer").hover(
    function () {
        $("#expertoffer img").attr("src","image/expert-fill/expert-fill.png")
            .attr("srcset","image/expert-fill/expert-fill@2x.png 2x,image/expert-fill/expert-fill@3x.png 3x");
    },
    function () {
        $("#expertoffer img").attr("src","image/expert-stroke/expert-stroke.png")
            .attr("srcset","image/expert-stroke/expert-stroke@2x.png 2x,image/expert-stroke/expert-stroke@3x.png 3x");
    });
$("#buyeroffer").hover(
    function () {
        $("#buyeroffer img").attr("src","image/buyer-fill/buyer-fill.png")
            .attr("srcset","image/buyer-fill/buyer-fill@2x.png 2x,image/buyer-fill/buyer-fill@3x.png 3x");
    },
    function () {
        $("#buyeroffer img").attr("src","image/buyer-stroke/buyer-stroke.png")
            .attr("srcset","image/buyer-stroke/buyer-stroke@2x.png 2x,image/buyer-stroke/buyer-stroke@3x.png 3x");
    });

//grid list
$('#list').click(function(event){
        event.preventDefault();
        $('#list img').attr("src","image/list-view-selected/list-view-selected.png")
            .attr("srcset","image/list-view-selected/list-view-selected@2x.png 2x,image/list-view-selected/list-view-selected@3x.png 3x");
        $('#grid img').attr("src","image/grid-view/grid-view.png")
        .attr("srcset","image/grid-view/grid-view@2x.png 2x,image/grid-view/grid-view@3x.png 3x");
        $('#products .item').removeClass('grid-group-item');
        $('#products .item').addClass('list-group-item');
        $('#products .grid-fix').addClass('col-md-6');
});

$('#grid').click(function(event){
        event.preventDefault();
        $('#list img').attr("src","image/list-view/list-view.png")
        .attr("srcset","image/list-view/list-view@2x.png 2x,image/list-view/list-view@3x.png 3x");
        $('#grid img').attr("src","image/grid-view-selected/grid-view-selected.png")
        .attr("srcset","image/grid-view-selected/grid-view-selected@2x.png 2x,image/grid-view-selected/grid-view-selected@3x.png 3x");
        $('#products .item').removeClass('list-group-item');
        $('#products .item').addClass('grid-group-item');
        $('#products .grid-fix').removeClass('col-md-6');
        $('#products .grid-fix').addClass('col-md-12');
});

//list item icon change on mouse over
$('.loc-hov').hover(
    function () {
        $('.loc-hov').children().attr("src","image/location-hover/location-hover.png")
            .attr("srcset","image/location-hover/location-hover@2x.png 2x,image/location-hover/location-hover@3x.png 3x");
    },
    function () {
        $('.loc-hov').children().attr("src","image/location/location.png")
            .attr("srcset","image/location/location@2x.png 2x,image/location/location@3x.png 3x");
    });

$('.cat-hov').hover(
    function () {
        $('.cat-hov').children().attr("src","image/category-hover/category-hover.png")
            .attr("srcset","image/category-hover/category-hover@2x.png 2x,image/category-hover/category-hover@3x.png 3x");
    },
    function () {
        $('.cat-hov').children().attr("src","image/category/category.png")
            .attr("srcset","image/category/category@2x.png 2x,image/category/category@3x.png 3x");
    });

//show more functionality
$(document).ready(function(){

    var list = $(".list-group .show-more .item");
    var numToShow = 3;
    var button = $("#next");
    var numInList = list.length;
    list.hide();
    if (numInList > numToShow) {
        button.show();
    }
    list.slice(0, numToShow).show();

    button.click(function(event){
        event.preventDefault();
        var showing = list.filter(':visible').length;
        list.slice(showing - 1, showing + numToShow).fadeIn();
        var nowShowing = list.filter(':visible').length;
        if (nowShowing >= numInList) {
            button.hide();
        }
    });

});

//max characters to show in info box description
$(function(){
    $(".info-box-description").each(function(i){
        len=$(this).text().length;
        if(len>80)
        {
            $(this).text($(this).text().substr(0,250));
        }
    });
});


$(function(){
    $(".project-back p").each(function(i){
        document.querySelector('p').classList.toggle('ow');
    }, false);
});

$(function(){
    $(".project-front p").each(function(i){
        document.querySelector('p').classList.toggle('ow');
    }, false);
});

String.prototype.filename=function(extension){
    var s= this.replace(/\\/g, '/');
    s= s.substring(s.lastIndexOf('/')+ 1);
    return extension? s.replace(/[?#].+$/, ''): s.split('.')[0];
}

$(document).ready(function() {
    var IDs = [];
    $(".sd-slider").find("img").each(function(){ IDs.push(this.id); });
    for (var i=0; i < IDs.length; i++) {
        IDs[i] = ("#").concat(IDs[i]);
    }

    $(IDs.join(", ")).hover(
        function () {
            $(this).attr("src", ("image/logo/hover/").concat($(this).attr('src').filename()).concat(".png"));
        }
        ,
        function () {
            $(this).attr("src", ("image/logo/").concat($(this).attr('src').filename()).concat(".png"));
        }
    );
    var f = 0;
    $("#sd-button").click(
        function () {
            if (f == 0) {
                $('.sd-container').slideUp("slow", function () {
                    $('.sd-container').hide();
                });
                $("#sd-button").html("⌄").css("color","#fff");
                f = 1;
            } else {
                $('.sd-container').slideDown("slow", function () {
                    $('.sd-container').show();
                });
                $("#sd-button").html("^").css("color","#fff");
                f = 0;
            }
        }
    );
});

$('.singup-box .singup-box-dropdown button').click(function () {
    $(this).html(function () {
        $('.singup-box .singup-box-dropdown ul li a').click(function () {
            return $(this).html();
        });
    });
});

$(function() {
// OPACITY OF BUTTON SET TO 0%
    $(".roll").css("opacity","0");
    $('.image-slider .slick-prev').css("opacity","0");
    $('.image-slider .slick-next').css("opacity","0");

// ON MOUSE OVER

    $(".roll").hover(function () {

// SET OPACITY TO 70%
            $(this).stop().animate({
                opacity: .7
            }, "fast");
            $('.image-slider .slick-prev').stop().animate({
                opacity: .7
            }, "fast");
            $('.image-slider .slick-next').stop().animate({
                opacity: .7
            }, "fast");
        },


// ON MOUSE OUT
        function () {

// SET OPACITY BACK TO 50%
            $(this).stop().animate({
                opacity: 0
            }, "slow");
            $('.image-slider .slick-prev').stop().animate({
                opacity: 0
            }, "slow");
            $('.image-slider .slick-next').stop().animate({
                opacity: 0
            }, "slow");
        });
});

$(document).ready(function() {
    $('.image-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        infinite: false,
        arrows: true,
        asNavFor: '.slider-nav-thumbnails'
    });
    $('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: false,
        asNavFor: '.slider-nav-thumbnails',
    });

    $('.slider-nav-thumbnails').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider',
        dots: false,
        focusOnSelect: false,
        arrows: false,
        infinite: false,
        accessibility: false
    });

//remove active class from all thumbnail slides
    $('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');

//set active class to first thumbnail slides
    $('.slider-nav-thumbnails .slick-slide').eq(0).addClass('slick-active');

// On before slide change match active thumbnail to current slide
    $('.slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var mySlideNumber = nextSlide;
        $('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');
        $('.slider-nav-thumbnails .slick-slide').eq(mySlideNumber).addClass('slick-active');
    });
    $('.slider-nav-thumbnails .slick-slide').on('click', function (event) {
        $('.slider').slick('slickGoTo', $(this).data('slickIndex'));
    });

//UPDATED

    $('.slider').on('afterChange', function(event, slick, currentSlide){
        $('.content').hide();
        $('.content[data-id=' + (currentSlide + 1) + ']').show();
    });


        $(".project-detail").slick({
            slidesToShow: 1,
            arrows: true,
            asNavFor: '.project-strip',
            autoplay: true,
            autoplaySpeed: 3000,
            dots: true
        });

        $(".project-strip").slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: false,
            asNavFor: '.project-detail',
            dots: false,
            infinite: true,
            centerMode: false,
            focusOnSelect: true
        });
        $('.sd-slider').slick({
            slidesToShow: 7,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 1024,
                unslick: true,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: true,
                    autoplay: true,
                    autoplaySpeed: 3000
                }
            }],
        });

});

//modal message status

$('.getmessage button').click(function () {
    $('.getmessage').hide();
    $('.sending').show();
    $('.close-pop_up').css("top","-35px");
    setTimeout(function () {
        $('.sending').hide();
        $('.sent').show();
        setTimeout(function () {
            $('.sent h4').css("margin", "28px auto 0");
            $('.sent .sent-btns').show();
        },3000);
    }, 5000);

});


//extendable share button
$(document).ready(function() {
    var f = 0;
    $('#share .sh-cls').click(function (event) {
        if (f == 0) {
            event.preventDefault();
            $('#share').fadeOut("fast", function () {
                $('#share').removeClass("open-share")
                    .addClass("share");
                $('#share .sh-cls').addClass("close-share");
                $('#share').animate({
                    width: "toggle",
                    height: "40px"
                }, 400, function () {
                    $('#share').show();
                });
            });
            f=1;
        }
        else  {
            event.preventDefault();
            $('#share').fadeOut("fast", function () {
                $('#share').removeClass("share")
                    .addClass("open-share");
                $('#share .sh-cls').removeClass("close-share");
                $('#share').hide();
                $('#share').fadeIn("fast",function () {
                    $('#share').show();
                });
            });
            f=0;
        }
    });
});
//login country field
if($("#country").length != 0) {
    $("#country").countrySelect();
}


//share button box hover
//twitter buttonn
$('.share-btns .twitter-btn').hover(
    function () {
        $('.share-btns .twitter-btn').children().attr("src","image/twitter-hover/twitter-hover.png")
            .attr("srcset","image/twitter-hover/twitter-hover@2x.png 2x,image/twitter-hover/twitter-hover@3x.png 3x");
    },
    function () {
        $('.share-btns .twitter-btn').children().attr("src","image/twitter-m-dark/twitter-m-dark.png")
            .attr("srcset","image/twitter-m-dark/twitter-m-dark@2x.png 2x,image/twitter-m-dark/twitter-m-dark@3x.png 3x");
    });
//facebook button
$('.share-btns .facebook-btn').hover(
    function () {
        $('.share-btns .facebook-btn').children().attr("src","image/fb-hover/fb-hover.png")
            .attr("srcset","image/gplus-hover/gplus-hover@2x.png 2x, image/gplus-hover/gplus-hover@3x.png 3x");
    },
    function () {
        $('.share-btns .facebook-btn').children().attr("src","image/fb-m-dark/fb-m-dark.png")
            .attr("srcset","image/twitter-hover/twitter-hover@2x.png 2x,image/twitter-hover/twitter-hover@3x.png 3x");
    });
//gplus button
$('.share-btns .gplus-btn').hover(
    function () {
        $('.share-btns .gplus-btn').children().attr("src","image/gplus-hover/gplus-hover.png")
            .attr("srcset","image/twitter-hover/twitter-hover@2x.png 2x,image/twitter-hover/twitter-hover@3x.png 3x");
    },
    function () {
        $('.share-btns .gplus-btn').children().attr("src","image/gplus-m-dark/gplus-m-dark.png")
            .attr("srcset","image/gplus-m-dark/gplus-m-dark@2x.png 2x, image/gplus-m-dark/gplus-m-dark@3x.png 3x");
    });
//faq selection functionaliti (vertical tabs)
//faq click functionality
$(document).ready(function(){

    $('#tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('#tabs li').removeClass('selected');
        $('.tab-content').hide();

        $(this).addClass('selected');
        $("#"+tab_id).show();
    })

})
//message delete hover
$('.message .delete').hover(
    function () {
        $('.message .delete').children().attr("src","image/delete-message-hover/delete-message-hover.png")
            .attr("srcset","image/delete-message-hover/delete-message-hover@2x.png 2x,image/delete-message-hover/delete-message-hover@3x.png 3x");
    },
    function () {
        $('.message .delete').children().attr("src","image/delete-message/delete-message.png")
            .attr("srcset","image/delete-message/delete-message@2x.png 2x, image/delete-message/delete-message@3x.png 3x");
    });
//message reply hover
$('.message .reply').hover(
    function () {
        $('.message .reply').children().attr("src","image/reply-message-hover/reply-message-hover.png")
            .attr("srcset","image/reply-message-hover/reply-message-hover@2x.png 2x,image/reply-message-hover/reply-message-hover@3x.png 3x");
    },
    function () {
        $('.message .reply').children().attr("src","image/reply-message/reply-message.png")
            .attr("srcset","image/reply-message/reply-message@2x.png 2x, image/reply-message/reply-message@3x.png 3x");
    });

//on reply message button click
$('.message .reply').click(function () {
    $('.message-container .reply-content-hidden').fadeIn("slow", function () {
        $(this).show();
    });
});
$('.message-content .reply-cancel').click(function () {
    $('.message-container .reply-content-hidden').fadeOut("slow", function () {
        $(this).hide();
    });
});

//send message animation

$('.message .reply-send').click(function () {
    $('.reply-btn-group, .message-input').hide();
    $('.sending').show();
    setTimeout(function () {
        $('.sending').hide();
        $('.sent').show();
        setTimeout(function () {
            $('.sent').show();
        },3000);
    }, 5000);

});