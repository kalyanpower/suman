<?php
/**
 * Template Name: Testimonial 
 * @package Avada
 * @subpackage Templates
 * 
 */
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
get_header(); ?>
<section id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
		<?php while(have_posts()) : ?>
		<?php the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="post-content">
			<?php the_content(); ?>
			<?php 
			fusion_link_pages(); 
			$title = get_the_title();
			?>
			<h2 class="testimonial_title"><?php echo $title; ?></h2>
			</div>
		</div>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
	
	<!--------------- Testimonial section starts from here ----------------------->
	
		<?php 
		global $wpdb;
		$args = array( 'post_type' => 'testimonial', 'posts_per_page' => 10 );
		$loop = new WP_Query($args);
		while($loop->have_posts()) : $loop->the_post();
					$id = get_the_ID();
					$value = get_field("image");
					$designation = get_field("designation");
					$facebook = get_field("facebook_link");
					$linedin = get_field("linkedin");
					$twitter = get_field("twitter");
					$excerpt = get_the_excerpt();
					$content = substr($excerpt, 0, 500);
					?>
					<div class="project">
						<div class="project-back">
                                <div class="col-md-5 no-padding">
                                <img class="person-img" src="<?php echo $value['url']; ?>" alt=""/>
								<div class="social_icons"><a href="<?php echo $facebook; ?>"><img src="<?php echo get_site_url();?>/wp-content/uploads/2018/03/facebook.png"></a><a href="<?php echo $twitter; ?>"><img src="<?php echo get_site_url();?>/wp-content/uploads/2018/03/twitter.png"></a><a href="<?php echo $linedin; ?>"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/2018/03/insta.png"></a></div>
                                </div>
                                <div class="col-md-7 no-padding">
								<blockquote>
								<div class="test-hed"><h3><?php the_title();?></h3>
								<h5><?php echo $designation;?></h5>
								<?php echo '<div class="entry-content">' . wp_trim_words( get_the_content(), 80 ) . '</div></div>';?>
								</blockquote>
                                </div>
                        </div>
                   </div>
                <?php endwhile; ?>
</section>
<?php do_action( 'avada_after_content' ); ?>
<?php
get_footer();