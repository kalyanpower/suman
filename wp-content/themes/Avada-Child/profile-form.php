<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
?>
<style>
.tml {
    max-width: 100%;
}
</style>
<link rel="stylesheet" href="<?php echo bloginfo('stylesheet_directory'); ?>/css/edit_profile/style-new.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo  bloginfo('stylesheet_directory'); ?>/css/edit_profile/bootstrap.css" type="text/css" media="screen" />

<script type="text/javascript" src="<?php echo  bloginfo('stylesheet_directory'); ?>/js/edit_profile/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo  bloginfo('stylesheet_directory'); ?>/js/edit_profile/scripts.js"></script>



 <div class="backimage a_container">
 <div class="container">
 <div class="edit-profile-sec col-md-6 col-sm-8 col-md-offset-3 col-sm-offset-2">

<div class="tml tml-profile" id="theme-my-login<?php $template->the_instance(); ?>">
	<?php $template->the_action_template_message( 'profile' ); ?>
	<?php $template->the_errors(); ?>
	
	
	
	<form id="your-profile" action="<?php $template->the_action_url( 'profile', 'login_post' ); ?>" method="post">
		<?php wp_nonce_field( 'update-user_' . $current_user->ID ); ?>

		<input type="hidden" name="from" value="profile" />
		<input type="hidden" name="checkuser_id" value="<?php echo $current_user->ID; ?>" />

		<?php if ( apply_filters( 'show_admin_bar', true ) || has_action( 'personal_options' ) ) : ?>
			

			<table class="tml-form-table">
			
			<?php do_action( 'personal_options', $profileuser ); ?>
			</table>
		<?php endif; ?>

		<?php do_action( 'profile_personal_options', $profileuser ); ?>
<h2>Edit Profile</h2>

		 <h5 class="">PERSONAL INFORMATION</h5>
			<div class="row">
				<table class="tml-form-table">
		
				<div class="col-sm-6">
			      <div class="form-group">
				  <label for="display_name"><?php _e( 'User Name ', 'theme-my-login' ); ?></label>
					<input type="text" name="user_login" id="user_login" value="<?php echo esc_attr( $profileuser->user_login ); ?>" disabled="disabled" class="regular-text" /> 
				  </div>
				 </div>
				 
				 
				 
				 <div class="col-sm-6">
			      <div class="form-group">
<label for="display_name"><?php _e( 'First Name', 'theme-my-login' ); ?></label>				  
					<input type="text" name="first_name" id="first_name" value="<?php echo esc_attr( $profileuser->first_name ); ?>" class="regular-text" />
				  </div>
				 </div>
				 
				 <div class="col-sm-6">
			      <div class="form-group">	
<label for="display_name"><?php _e( 'Last Name', 'theme-my-login' ); ?></label>				  
					<input type="text" name="last_name" id="last_name" value="<?php echo esc_attr( $profileuser->last_name ); ?>" class="regular-text" />
				  </div>
				 </div>
		
				<div class="col-sm-6">
			      <div class="form-group">	
<label for="display_name"><?php _e( 'Email', 'theme-my-login' ); ?></label>				  
					<input type="text" name="email" id="email" value="<?php echo esc_attr( $profileuser->user_email ); ?>" class="regular-text" />
					<?php
					$new_email = get_option( $current_user->ID . '_new_email' );
					if ( $new_email && $new_email['newemail'] != $current_user->user_email ) : ?>
					<div class="updated inline">
					<p><?php
				printf(
					__( 'There is a pending change of your e-mail to %1$s. <a href="%2$s">Cancel</a>', 'theme-my-login' ),
					'<code>' . $new_email['newemail'] . '</code>',
					esc_url( self_admin_url( 'profile.php?dismiss=' . $current_user->ID . '_new_email' ) )
					); ?></p>
					</div>
					<?php endif; ?>
				  </div>
				 </div>
				 
				  <div class="col-sm-6">
			      <div class="form-group">	
<label for="display_name"><?php _e( 'Password', 'theme-my-login' ); ?></label>				  
						<input type="password" name="pass1" id="pass1" class="regular-text" value="<?php echo esc_attr( $profileuser->password ); ?>" autocomplete="off" data-pw="<?php echo esc_attr( wp_generate_password( 24 ) ); ?>" aria-describedby="pass-strength-result" placeholder="password"/>
				  </div>
				 </div>
				 
				  <div class="col-sm-6">
			      <div class="form-group">
<label for="display_name"><?php _e( 'Confirm Password', 'theme-my-login' ); ?></label>				  
						<input type="password" name="pass2" id="pass2" class="regular-text" value="<?php echo esc_attr( $profileuser->confirm_password ); ?>" autocomplete="off" data-pw="<?php echo esc_attr( wp_generate_password( 24 ) ); ?>" aria-describedby="pass-strength-result" placeholder="confirm password" />
				  </div>
				 </div>
				
		</table>

		</div>
		
		
 <h5 class="">Contact Info</h5>
				  <div class="row">
				   <div class="col-sm-6">
					  <div class="form-group">		
<label for="display_name"><?php _e( 'Company Name', 'theme-my-login' ); ?></label>					  
						<input type="text" name="company_name" id="company_name" value="<?php echo esc_attr( $profileuser->company_name ); ?>" class="regular-text" />
					  </div>
					 </div>
					 
					
				   <div class="col-sm-6">
					  <div class="form-group">	
<label for="display_name"><?php _e( 'Phone', 'theme-my-login' ); ?></label>					  
						<input type="text" name="phone" id="phone" value="<?php echo esc_attr( $profileuser->phone ); ?>" placeholder="Phone" class="regular-text code" />
					  </div>
					 </div>
					 
					
				   <div class="col-sm-6">
					  <div class="form-group">
<label for="display_name"><?php _e( 'Asddress', 'theme-my-login' ); ?></label>					  
						<input type="text" name="address" id="address" value="<?php echo esc_attr( $profileuser->address ); ?>" placeholder="Address" class="regular-text" />
					  </div>
					 </div>
					 
					 
					 <div class="col-sm-6">
					  <div class="form-group">	
<label for="display_name"><?php _e( 'Zip Code', 'theme-my-login' ); ?></label>					  
						<input type="text" name="zip_code" id="zip_code" value="<?php echo esc_attr( $profileuser->zip_code ); ?>" placeholder="Zip Code" class="regular-text" />
					  </div>
					 </div>
					 
					 
					 <div class="col-sm-6">
					  <div class="form-group">	
<label for="display_name"><?php _e( 'City', 'theme-my-login' ); ?></label>					  
						<input type="text" name="city" id="city" value="<?php echo esc_attr( $profileuser->city ); ?>" placeholder="City" class="regular-text" />
					  </div>
					 </div>
  
  
               <div class="col-sm-6">
					  <div class="form-group">		
<label for="display_name"><?php _e( 'Country', 'theme-my-login' ); ?></label>					  
						 <select name="country" id="display_name">
				<?php
					$public_display = array();
					//$public_display['display_nickname']  = $profileuser->nickname;
					$public_display['display_username']  = $profileuser->user_login;

					if ( ! empty( $profileuser->first_name ) )
						$public_display['display_firstname'] = $profileuser->first_name;

					if ( ! empty( $profileuser->last_name ) )
						$public_display['display_lastname'] = $profileuser->last_name;

					if ( ! empty( $profileuser->first_name ) && ! empty( $profileuser->last_name ) ) {
						$public_display['display_firstlast'] = $profileuser->first_name . ' ' . $profileuser->last_name;
						$public_display['display_lastfirst'] = $profileuser->last_name . ' ' . $profileuser->first_name;
					}

					if ( ! in_array( $profileuser->display_name, $public_display ) )// Only add this if it isn't duplicated elsewhere
						$public_display = array( 'display_displayname' => $profileuser->display_name ) + $public_display;

					$public_display = array_map( 'trim', $public_display );
					$public_display = array_unique( $public_display );

					foreach ( $public_display as $id => $item ) {
				?>
					<option <?php selected( $profileuser->display_name, $item ); ?>><?php echo "India"; //echo $item; ?></option>
				<?php
					}
				?>
				</select>
					  </div>
					 </div>
                    
				
  
		<table class="tml-form-table">
		

		<?php
		$show_password_fields = apply_filters( 'show_password_fields', true, $profileuser );
		if ( $show_password_fields ) :
		?>
		</table>

		
		<table class="tml-form-table">
		
		
		<tr class="pw-weak">
			<th><?php _e( 'Confirm Password', 'theme-my-login' ); ?></th>
			<td>
				<label>
					<input type="checkbox" name="pw_weak" class="pw-checkbox" />
					<?php _e( 'Confirm use of weak password', 'theme-my-login' ); ?>
				</label>
			</td>
		</tr>
		<?php endif; ?>

		</table>
		
		<?php do_action( 'show_user_profile', $profileuser ); ?>

		<p class="tml-submit-wrap">
			<input type="hidden" name="action" value="profile" />
			<input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
			<input type="hidden" name="user_id" id="user_id" value="<?php echo esc_attr( $current_user->ID ); ?>" />
			<input type="submit" class="button-primary btn reg-btn" value="<?php esc_attr_e( 'Update Profile', 'theme-my-login' ); ?>" name="submit" id="submit" />
			
		</p>
		</div>
	</form>
</div>
</div>
</div>
</div>
