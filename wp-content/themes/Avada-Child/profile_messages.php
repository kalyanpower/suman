<?php
/**
 * Template name: Profile Message
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); 

if (is_user_logged_in()){
    $userid = get_current_user_id();

    $table_name = $wpdb->prefix . "ads_message"; 
    $query = $wpdb->get_results("SELECT * from $table_name WHERE userid='$userid' ORDER BY ads_msg_id DESC");
  /*  echo"<pre>";
    print_r($query);
    echo"</pre>";*/

    $table_name2 = $wpdb->prefix . "users";
    $table_name3 = $wpdb->prefix . "usermeta";

    $addClass ="";
       

?>
<input type="hidden" name="userid" id="userid" class="userid" value="<?php echo $userid; ?>" />

<div class="container">
    <div class="col-md-8">
        <div class="banner-620px-80px">
            <button class="close-banner"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/close-banner/close-banner.png" srcset="images/close-banner/close-banner@2x.png 2x, images/close-banner/close-banner@3x.png 3x"></button>
            <div class="text-area">
                <p>Get Noticed!</p>
                <p>Become a premium user!</p>
            </div>
            <button class="banner-btn btn btn-lg"><a href="<?php echo site_url('/payment') ?>">UPGRADE</a></button>
        </div>
        <div class="message-container"> 
        <?php 

        foreach ($query as $queryvalue) { 

        if($queryval->read_status==0){
            $addClass ="unread";
        }else{
            $addClass ="";
        }

        $date1 = strtotime($queryvalue->message_created_at);
        $date2 = strtotime(date('Y-m-d H:i:s'));
        $seconds_diff = $date2 - $date1;

        //echo $agoTime = round(abs($seconds_diff) / 3600,2). " hours ago";
        if($seconds_diff>=60){
            $agoTime = round(abs($seconds_diff) / 60). " minute ago";
        
        }
        if($seconds_diff>=3600){
            $agoTime = round(abs($seconds_diff) / 3600). " hours ago";
        }
        if($seconds_diff>=86400){
            $agoTime = round(abs($seconds_diff) / 86400). " Day ago";
        }
        if($seconds_diff>=2592000){
            $agoTime = round(abs($seconds_diff) / 86400). " month ago";
        }
        if($seconds_diff>=31104000){
            $agoTime = round(abs($seconds_diff) / 86400). " Year ago";
        }

             $query2 = $wpdb->get_results("SELECT * from $table_name2 JOIN $table_name3 ON ($table_name2.ID = $table_name3.user_id) where $table_name2.ID='$queryvalue->sender_id'");

             if($query2[0]->user_login=='admin'){
            $company = $query2[48]->meta_value;
        }else{
            $company = $query2[13]->meta_value;
        }
    
        ?>
       
        <div class="message <?php echo $addClass; ?>">
                <div class="sender-info">
                    <a href="<?php echo esc_url( add_query_arg( 'id', $queryvalue->ads_msg_id, get_permalink( 405 ) ) ); ?>">
                        <img src="http://placehold.it/34x34/000/fff" class="sender-pf-pic">
                        <div class="text-block">
                            <h5 class="sender-name"><?php echo $query2[0]->display_name; ?></h5>
                            <h6 class="sender-pro"><?php echo $company; ?></h6>
                        </div>
                    </a>
                </div>
                <div class="message-time">
                    <h6 class="time"><?php echo $agoTime; ?></h6>
                    <h6 class="message-count"></h6>
                </div>
                <div class="message-content">
                    <p class="message-prev"><?php echo mb_strimwidth($queryvalue->message, 0, 150, "..."); ?></p>
                    <button class="reply"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/reply-message/reply-message.png" srcset="image/reply-message/reply-message@2x.png 2x, image/reply-message/reply-message@3x.png 3x"></button>
                    <button class="delete"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/delete-message/delete-message.png" srcset="image/delete-message/delete-message@2x.png 2x, image/delete-message/delete-message@3x.png 3x"></i></button>
                </div>
            </div>   
        <?php   }  ?>

        </div>

    </div>
<div class="modal fade" id="Modal_rply" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content"> 
    <form method="post" name="replyForm" id="replyForm">        
      <div class="modal-body">
          <div class="getmessage">
            <h4>SEND MESSAGE</h4> 
            <input type="hidden" name="msgId" id="messgae-id" value="">
            
            <textarea class="form-control col-xs-12" name="rply_name" id="rply_name" required></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Reply </button>
      </div>
  </form>
    </div>
  </div>
</div>






</div>
<?php do_action( 'avada_after_content' ); 
}
else
{
echo '<h3 class="logged_msg">You are not authorized to view the content on this page.</h3>';
}

get_footer();
?>

<script type="text/javascript" >
jQuery(document).ready(function(){
    
   var funky = setInterval(function(){ recentmessage() }, 1000);

    function recentmessage(){

        var userid = jQuery('#userid').val();
        jQuery.ajax({
            url: "<?php echo admin_url('admin-ajax.php'); ?>",
            type: 'POST',
            data: { userid: userid, intVal: funky,
            action: 'getMessages'
            },
            dataType: 'json',
            success: function(response) {
                if(response==0){

                }else{
                    jQuery('.message-container').html(response);
                }
            }              
        });
    }
});
</script>
<script type="text/javascript">
jQuery("#replyForm").submit(function(e) {
   e.preventDefault();
   var message = jQuery('textarea#rply_name').val();
   var userid = jQuery('#userid').val();
   var ads_id = jQuery('#ads_id').val();
   var reciver_id = jQuery('#reciver_id').val();

  jQuery.ajax({
       type: "POST",
       url: '<?php echo admin_url( 'admin-ajax.php' );?>',
       data: {message: message,userid: userid,ads_id:ads_id,reciver_id: reciver_id, action:'MessageReply'},
       success: function(data){            
            if(data ==1){

               // alert("hi"+data);
                alert("Message was sent successfully");
                jQuery("#Modal_rply").delay(1000).fadeOut(800);
                window.location.href = "<?php echo site_url('/inbox-message');?>";
                
            }else{
                //alert("hello"+data);
               alert("Message was not sent");
                
            }
        }
    });
  
});
</script>