<?php
/**
 * Header-3 template.
 *
 * @author     ThemeFusion
 * @copyright  (c) Copyright by ThemeFusion
 * @link       http://theme-fusion.com
 * @package    Avada
 * @subpackage Core
 */
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<div class="fusion-header-sticky-height"></div>
<div class="fusion-header">
	<div class="fusion-row">
		<div class="fusion-header-v6-content fusion-header-has-flyout-menu-content">
			<div class="fusion-flyout-menu-icons">
				<?php if ( class_exists( 'WooCommerce' ) && Avada()->settings->get( 'woocommerce_cart_link_main_nav' ) ) : ?>
					<?php
					global $woocommerce;

					$cart_link_text  = '';
					$cart_link_class = '';
					if ( Avada()->settings->get( 'woocommerce_cart_counter' ) && $woocommerce->cart->get_cart_contents_count() ) {
						$cart_link_text  = '<span class="fusion-widget-cart-number">' . $woocommerce->cart->get_cart_contents_count() . '</span>';
						$cart_link_class = ' fusion-widget-cart-counter';
					}
				?>
					<div class="fusion-flyout-cart-wrapper">
						<a href="<?php echo esc_attr( get_permalink( get_option( 'woocommerce_cart_page_id' ) ) ); ?>" class="fusion-icon fusion-icon-shopping-cart<?php echo esc_attr( $cart_link_class ); ?>" aria-hidden="true" aria-label="<?php esc_attr_e( 'Toggle Shopping Cart', 'Avada' ); ?>"><?php echo $cart_link_text; // WPCS: XSS ok. ?></a>
					</div>
				<?php endif; ?>

				<?php if ( 'menu' === Avada()->settings->get( 'slidingbar_toggle_style' ) && Avada()->settings->get( 'slidingbar_widgets' ) ) : ?>
					<?php $sliding_bar_label = esc_attr__( 'Toggle Sliding Bar', 'Avada' ); ?>
					<div class="fusion-flyout-sliding-bar-toggle">
						<a href="#" class="fusion-toggle-icon fusion-icon fusion-icon-sliding-bar" aria-label="<?php echo esc_attr( $sliding_bar_label ); ?>"></a>
					</div>
				<?php endif; ?>

				<?php if ( Avada()->settings->get( 'main_nav_search_icon' ) ) : ?>
					<div class="fusion-flyout-search-toggle">
						<div class="fusion-toggle-icon">
							<div class="fusion-toggle-icon-line"></div>
							<div class="fusion-toggle-icon-line"></div>
							<div class="fusion-toggle-icon-line"></div>
						</div>
						<a class="fusion-icon fusion-icon-search" aria-hidden="true" aria-label="<?php esc_attr_e( 'Toggle Search', 'Avada' ); ?>" href="#"></a>
					</div>
				<?php endif; ?>

				<a class="fusion-flyout-menu-toggle" aria-hidden="true" aria-label="<?php esc_attr_e( 'Toggle Menu', 'Avada' ); ?>" href="#">
					<div class="fusion-toggle-icon-line"></div>
					<div class="fusion-toggle-icon-line"></div>
					<div class="fusion-toggle-icon-line"></div>
				</a>
				<span class="m_title">MENU</span>
			</div>
			
			<?php
			avada_logo();
			$menu = avada_main_menu(true);
			?>
			<?php if(is_user_logged_in()){ ?>
			<?php 
				$userid = get_current_user_id();
				$table_name = $wpdb->prefix . "ads_message";
				$query = $wpdb->get_var("select count(*) from $table_name where receiver_id='$userid' and read_status='0'");
			?>
	<div class="header_right">
		<div class="cus-pop-right">
			<div class="notification_icon">
				<div id="search_box_form">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/icon/header-search.svg">
				</div>
				<div class="header_icon">
					<a href="#">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/icon/header-help.svg">
					</a>
				</div>		
				<div class="header_icon">
					<a href="<?php echo site_url(); ?>/inbox-message/">
						<span class="upper-num"><?php echo $query; ?></span>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/icon/header-notifications.svg">
					</a>

				</div>					
			</div>
		<div class="header-info pull-right">
			<div class="language">
				<div class="btn-group">
					<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button">PROFILE</button>
					<div class="dropdown dropdown-menu">
						<div class="image_sec">
							<div class="dummy_img">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/dummy.png">
							</div>
							<?php global $user; ?>
							<div class="img_txt">
								<h3><?php $current_user = wp_get_current_user();
								echo $current_user->user_firstname; ?>&nbsp;<?php echo $current_user->user_lastname; ?></h3>
								<a href="#"><?php echo $current_user->id; ?></a>
								<p>+41 (0) 44 123 45 67</p>
							</div>
							<div class="create-ad-now">
								<button type="button"><a href="<?php echo site_url(); ?>/add-advertisement/">Create ad now!</a></button>
							</div>
						</div>
						<div class="dropdown_txt">
							<ul>
							    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inbox.png"><span class="lft_txt"><a href="<?php echo site_url(); ?>/profile/">Profile</a></span><span class="right_txt">2</span></li>
								<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/inbox.png"><span class="lft_txt"><a href="<?php echo site_url(); ?>/inbox-message/">Inbox</a></span><span class="right_txt"><?php echo $query; ?></span></li>
								<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/my_ads.png"><span class="lft_txt"><a href="<?php echo site_url(); ?>/my-ads/">My ads</a></span><span class="right_txt">2</span></li>
								<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/my_ads.png"><span class="lft_txt"><a href="<?php echo site_url(); ?>/literature/">Literature</a></span><span class="right_txt">2</span></li>
								<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/my_ads.png"><span class="lft_txt"><a href="<?php echo site_url(); ?>/event_form/">Event</a></span><span class="right_txt">2</span></li>
								<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/my_post.png"><span class="lft_txt"><a href="#">My Posts</a></span><span class="right_txt">2</span></li>
								<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/contact.png"><span class="lft_txt"><a href="#">My Contact</a></span><span class="right_txt">2</span></li>
								<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/my_document.png"><span class="lft_txt"><a href="<?php echo site_url(); ?>/my-dcoument/">My Document</a></span><span class="right_txt">2</span></li>
								<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/adds_saved.png"><span class="lft_txt"><a href="<?php echo site_url(); ?>//save-ads/">Saved ads</a></span><span class="right_txt">2</span></li>
								<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/search3.png"><span class="lft_txt"><a href="<?php echo site_url(); ?>/save-search/">Saved search Presets</a></span><span class="right_txt">2</span></li>
								<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/edit.png"><span class="lft_txt"><a href="<?php echo site_url(); ?>/edit_profile/">Edit Profile</a></span></li>
								<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/log_out.png"><span class="lft_txt"><a href="#">Log out</a></span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			</div>
			</div>
			</div>
			<?php }
			else{ ?>
			<div class="header_right">
			<?php dynamic_sidebar('Header Right'); ?>
			</div>
			<?php } ?>
			
		</div>

		<div class="fusion-main-menu fusion-flyout-menu" role="navigation" aria-label="Main Menu">
			<?php echo $menu; // WPCS: XSS ok. ?>
		</div>

		<?php if ( Avada()->settings->get( 'main_nav_search_icon' ) ) : ?>
			<div class="fusion-flyout-search">
				<?php get_search_form(); ?>
			</div>
		<?php endif; ?>

		<div class="fusion-flyout-menu-bg"></div>
	</div>
</div>
<div class="row search_box" style="display: none;">

<!-- 	<form method="get" id="advanced-searchform" role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>">

    <h3><?php _e( 'Advanced Search', 'textdomain' ); ?></h3>

  
    <input type="hidden" name="search" value="advanced">

    <label for="s" class=""><?php _e( 'Name: ', 'textdomain' ); ?></label><br>
    <input type="text" value="" placeholder="<?php _e( 'Type the Car Name', 'textdomain' ); ?>" name="s" id="name" />

    <label for="model" class=""><?php _e( 'Select a Model: ', 'textdomain' ); ?></label><br>
    <select name="model" id="model">
        <option value=""><?php _e( 'Select one...', 'textdomain' ); ?></option>
        <option value="model1"><?php _e( 'Model 1', 'textdomain' ); ?></option>
        <option value="model2"><?php _e( 'Model 2', 'textdomain' ); ?></option>
    </select>

    <input type="submit" id="searchsubmit" value="Search" />

</form> -->

	<div class="form-group">
		<!-- <form id="autosearch_form" name="autosearch_form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>"> -->
		<form method="get" id="autosearch_form" role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
			<input type="hidden" name="search" value="advanced">
			<select class="form-control select_search" name="select_search" id="select_search" onchange="getval(this);">
			    <option value="all" selected>All</option>
				<option value="buyers">For buyers </option>
				<option value="sellers">For sellers</option>
				<option value="events">Events</option>
				<option value="literature">Literature</option>
				<option value="post">Blog</option>
				<option value="templates">Templates</option>
				<option value="drafts">Drafts</option>
				<option value="descriptions">Descriptions</option>
	  		</select>

	  		<input type="text" name="s" class="form-control input_search" id="input_search">
	  	
	  		<button type="submit" class="normal_search" name="normal_search">SEARCH</button>
  		</form>
  		<div class="search_filter">
  			<a class="advanced_search_form" href="javascript:void(0);">SHOW FILTERS</a>
  		</div>
  	</div>
  	<div class="advanced_fiter_form" style="display:none;">
		<form id="advanced_search" name="advanced_search" method="post">
			<div class="form-group">	
				<div class="container">	
					
					<?php


						$table_name = $wpdb->prefix . "advertisement";
						$query = $wpdb->get_results("select * from $table_name");

						/*echo"<pre>";
						print_r($query);
						echo"</pre>";*/
						//$table_name = $wpdb->prefix . "advertisement";


					 ?>

					 <?php 
					  	foreach ($query as $queryVal) {
					  		$ad_type[] = $queryVal->industry;
					  		$area_type= $queryVal->address;
					  		$area_types[] = explode(",",$area_type);
					  		
						}
						$ad_types = array_filter($ad_type);
						$ad_types_val = array_unique($ad_types);
						$area_type_vals = array_filter($area_types);
						$area_type_val = array_unique($area_type_vals);

					/*	echo"<pre>";
						print_r($area_type_val);
						echo"</pre>";*/


					?>

					<select class="form-control select_search_category" name="select_search_category" id="select_search_category">
						<option value="ad_type" selected>Ad type</option>
						<?php foreach ($ad_types_val as $ad_types_value) { ?>
													
						<option value="<?php echo $ad_types_value; ?>"><?php echo $ad_types_value; ?></option>	
						<?php } ?>					
					</select>
					<select class="form-control select_search_branch" name="select_search_branch" id="select_search_branch">
						<option value="branch" selected>Branch</option>	
						<?php foreach ($ad_types_val as $ad_types_value) { ?>
													
						<option value="<?php echo $ad_types_value; ?>"><?php echo $ad_types_value; ?></option>	
						<?php } ?>
					</select>
					<select class="form-control select_search_country" name="select_search_country" id="select_search_country">
						<option value="country" selected>Country</option>


						<?php 

							$table_name = $wpdb->prefix . "countries";
							$query = $wpdb->get_results("select * from $table_name");
							foreach ($query as $queryCountry) { 						
						?>

							<option value="<?php echo $queryCountry->id; ?>"><?php echo $queryCountry->country_name; ?></option>							
						<?php } ?>
					</select>
					<select class="form-control select_search_area" name="select_search_area" id="select_search_area" >
						<option value="area" selected>Area</option>
						<?php foreach ($area_type_val[0] as $area_type_value) { ?>
									
						<option value="<?php echo $area_type_value; ?>"><?php echo $area_type_value; ?></option>	
						<?php } ?>
						

					</select>
				</div>
				<div class="container">	
					<div class="form-group">

						<label for="amount">PRICE</label>
						<input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
						<div class="chf_range">
							<div class="min_chf">1CHF</div>
								<div id="slider-range"></div>
							<div class="max_chf">MAX CHF</div>
							<input type="hidden" name="amount1" class="amount1" id="amount1">
							<input type="hidden" name="amount2" class="amount2" id="amount2">
							<div class="tooltip tooltip1" id="tooltip1" style=" position:absolute;z-index:99;"></div>
							<div class="tooltip tooltip2" id="tooltip2" style=" position:absolute;z-index:99;"></div>	
						</div>
					</div>
				</div>
			</div>
			<div class="save_search_preset_btn">
				<div class="container">
					<p><strong>Want to save time on next search?</strong> Save search preset and access them from your profile the next time!</p>
					<button type="submit" class="advanced_search_preset" name="advanced_search_preset">SAVE SEARCH PRESET</button>
				</div>	    
			</div>	    
		</form>
	</div>
</div>

<?php 


if(isset($_POST['advanced_search_preset'])){
	$userid = get_current_user_id();

	$preset_ad_search = $_POST['select_search_category'];
	$preset_search_branch = $_POST['select_search_branch'];
	$preset_country = $_POST['select_search_country'];
	$preset_search_area = $_POST['select_search_area'];
	$min_amount=$_POST['amount1'];
	$max_amount=$_POST['amount2'];

	$table_name = $wpdb->prefix . "save_preset_search";
		
	$query = $wpdb->insert($table_name, array('userid' => $userid ,'preset_ad_search' => $preset_ad_search,'preset_search_branch'=>$preset_search_branch,'preset_country' =>$preset_country,'preset_search_area' =>$preset_search_area,'min_amount' =>$min_amount,'max_amount' =>$max_amount));	
	if($query)
	{
	$msg="Search Preset was saved successfully";
	echo '<h5 class="ad_success_msg">' . $msg.'</h5>';
	}
	else{
	echo 'There is some prolem found. Please try again later.';
	}
}



?>

