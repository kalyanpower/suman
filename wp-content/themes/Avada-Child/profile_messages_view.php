<?php
/**
 * Template name: Inbox Message Single View
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); 

if (is_user_logged_in()){
    $userid = get_current_user_id();

    $msg_ID = $_GET['id'];

    $table_name = $wpdb->prefix . "ads_message"; 
    $query = $wpdb->get_row("SELECT * from $table_name WHERE ads_msg_id='$msg_ID'");
    

    $table_name2 = $wpdb->prefix . "users";
    $table_name3 = $wpdb->prefix . "usermeta";

    $query2 = $wpdb->get_results("SELECT * from $table_name2 JOIN $table_name3 ON ($table_name2.ID = $table_name3.user_id) where $table_name2.ID='$query->sender_id'");

     if($query2[0]->user_login=='admin'){
            $company = $query2[48]->meta_value;
        }else{
            $company = $query2[13]->meta_value;
        }
    
   /* echo"<pre>";
    print_r($query);
    echo"</pre>";*/

?>
<input type="hidden" name="userid" id="userid" class="userid" value="<?php echo $userid; ?>" />

<div class="container">
    <div class="col-md-8">
        <div class="banner-620px-80px">
            <button class="close-banner"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/close-banner/close-banner.png" srcset="images/close-banner/close-banner@2x.png 2x, images/close-banner/close-banner@3x.png 3x"></button>
            <div class="text-area">
                <p>Get Noticed!</p>
                <p>Become a premium user!</p>
            </div>
            <button class="banner-btn btn btn-lg"><a href="<?php echo site_url('/payment') ?>">UPGRADE</a></button>
        </div>
        <div class="message-container">
            <div class="back-to-btn">
                <a href="<?php echo site_url('/inbox-message'); ?>" class="btn btn-sm back-to-btn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon/slick-previous.png">Back to Inbox</a>
            </div>
        <?php if($query){ ?>

       
            <div class="message single">
                <div class="sender-info">
                    <img src="http://placehold.it/34x34/000/fff" class="sender-pf-pic">
                    <div class="text-block">
                        <h5 class="sender-name"><?php echo $query2[0]->display_name; ?></h5>
                        <h6 class="sender-pro"><?php echo $company; ?></h6>
                    </div>
                </div>
                <div class="message-time">
                    <h6 class="time"><?php echo date('d-m-Y',strtotime($query->message_created_at)); ?></h6>
                    <h6 class="message-count"></h6>
                </div>
                <div class="message-content">
                    <p class="message-prev"><?php echo $query->message; ?></p>
                    <button class="reply" data-toggle="modal" data-target="#Modal_rply"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/reply-message/reply-message.png" srcset="image/reply-message/reply-message@2x.png 2x, image/reply-message/reply-message@3x.png 3x"></button>
                    <button class="delete" Onclick="ConfirmDeleteSingleMsg(<?php echo $query->ads_msg_id; ?>);"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/delete-message/delete-message.png" srcset="image/delete-message/delete-message@2x.png 2x, image/delete-message/delete-message@3x.png 3x"></button>
                </div>
            </div>
        <?php }else{ ?>

        <div class="no_records"> <h2>No Messages are to view</h2></div>
       <?php } ?>
        </div>
    </div>
<div class="modal fade" id="Modal_rply" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content"> 
    <form method="post" name="replyForm" id="replyForm">        
      <div class="modal-body">
          <div class="getmessage">
            <h4>SEND MESSAGE</h4> 
            <?php if($query){ ?>
            <input type="hidden" name="userid" id="userid" value="<?php echo $userid; ?>">
            <input type="hidden" name="ads_id" id="ads_id" class="ads_id" value="<?php echo $query->ads_id; ?>">
            <input type="hidden" name="reciver_id" id="reciver_id" class="reciver_id" value="<?php echo $query->sender_id; ?>">            
            <textarea class="form-control col-xs-12" name="rply_name" id="rply_name" required></textarea>
            <?php } ?>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Reply </button>
      </div>
  </form>
    </div>
  </div>
</div>

</div>
<?php do_action( 'avada_after_content' ); 
}
else
{
echo '<h3 class="logged_msg">You are not authorized to view the content on this page.</h3>';
}

get_footer();
?>

<script type="text/javascript" >
jQuery(document).ready(function(){
   //var funky = setInterval(function(){ recentmessage() }, 1000);

    function recentmessage(){

        var userid = jQuery('#userid').val();
        jQuery.ajax({
            url: "<?php echo admin_url('admin-ajax.php'); ?>",
            type: 'POST',
            data: { userid: userid, intVal: funky,
            action: 'getMessages'
            },
            dataType: 'json',
            success: function(response) {
                if(response==0){

                }else{
                    jQuery('.message-container').html(response);
                }
            }              
        });
    }
});
</script>
<script type="text/javascript">
function setReplySingleMessage(msgId){
        console.log(msgId);

        jQuery('#messgae-id').val(msgId);   
        //jQuery('#rply_name').val(msgId);
       // var messgae = jQuery('textarea#rply_name').val();
        
    }
jQuery("#replyForm").submit(function(e) {
   e.preventDefault();
   var message = jQuery('textarea#rply_name').val();
   var userid = jQuery('#userid').val();
   var ads_id = jQuery('#ads_id').val();
   var reciver_id = jQuery('#reciver_id').val();

  jQuery.ajax({
       type: "POST",
       url: '<?php echo admin_url( 'admin-ajax.php' );?>',
       data: {message: message,userid: userid,ads_id:ads_id,reciver_id: reciver_id, action:'MessageReply'},
       success: function(data){            
            if(data ==1){

                //alert("hi"+data);
                
                alert("Message was sent successfully");
                jQuery("#Modal_rply").delay(1000).fadeOut(800);
                window.location.href = "<?php echo esc_url( add_query_arg( 'id',$msg_ID , get_permalink( 405 ) ) ); ?>";
                
            }else{

                alert("Message was not sent");              
                
            }
        }
    });
  
});
</script>
<script type="text/javascript">   
    function ConfirmDeleteSingleMsg(msg_id){



        jQuery.ajax({
            url: "<?php echo admin_url('admin-ajax.php'); ?>",
            type: 'POST',
            data: { msg_id : msg_id,
            action: 'DeleteMessage'
            },
            beforeSend: function() {
        
                 var x = confirm("Are you sure you want to delete?");
                    if (x)
                    return true;
                    else
                    return false;
             },
            success: function(response) {

                //alert(response);
                if(response==0){
                    //alert("Message was deleted successfully");
                    window.location.href = "<?php echo site_url('/inbox-message');?>";
                }else{
                    //jQuery('.message').html(response);
                    alert("Message was not deleted");
                }
            }              
        });
    }

</script>