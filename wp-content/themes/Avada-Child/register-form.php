<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
global $wpdb;
$table_name = $wpdb->prefix . "membership_levels"; 
$query= $wpdb->get_results("SELECT * from $table_name where id IN (4,2,1)");
?>
<div class="tml_login_form">
<?php echo do_shortcode( '[theme-my-login instance="1"]' ); ?>
</div>
<div class="tml tml-register tml_register_form" id="theme-my-login<?php $template->the_instance(); ?>">
    <h1>SIGN UP</h1>
	<?php //$template->the_action_template_message( 'register' ); ?>
	<?php $template->the_errors(); ?>
	<h2>PERSONAL INFORMATION</h2>
	<form name="registerform" id="registerform<?php $template->the_instance(); ?>" action="<?php $template->the_action_url( 'register', 'login_post' ); ?>" method="post">
		<?php if ( 'email' != $theme_my_login->get_option( 'login_type' ) ) : ?>
		<p class="tml-user-login-wrap">
			<label for="user_login<?php $template->the_instance(); ?>"><?php _e( 'Username', 'theme-my-login' ); ?></label>
			<input type="text" name="user_login" id="user_login<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_login' ); ?>" size="20" placeholder="Username" tabindex="1"/>
		</p>
		<?php endif; ?>
        <p>
	    <label for="first_name<?php $template->the_instance(); ?>"><?php _e( 'First name', 'theme-my-login' ) ?></label>
		<input type="text" placeholder="First Name" name="first_name"  id="first_name<?php $template->the_instance();?>"  class="input" value="<?php $template->the_posted_value( 'first_name' ); ?>" size="20" tabindex="2" />
		</p>
		<p>
		<label for="last_name<?php $template->the_instance(); ?>"><?php _e( 'Last name', 'theme-my-login' ) ?></label>
		<input type="text" placeholder="Last Name"  name="last_name" id="last_name<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'last_name' ); ?>" size="20" tabindex="3" />
		</p>
		<p class="tml-user-email-wrap">
		<label for="user_email<?php $template->the_instance(); ?>"><?php _e( 'E-mail', 'theme-my-login' ); ?></label>
		<input type="text" placeholder="Email"  name="user_email" id="user_email<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_email' ); ?>" size="20" tabindex="4"/>
		</p>
		<?php do_action( 'register_form' ); ?>

		<p class="tml-registration-confirmation"  id="reg_passmail<?php $template->the_instance(); ?>"><?php echo apply_filters( 'tml_register_passmail_template_message', __( 'Registration confirmation will be e-mailed to you.', 'theme-my-login' ) ); ?></p>
		<h2>CONTACT INFORMATION</h2>
		<p>
		<label for="Company_name<?php $template->the_instance(); ?>"><?php _e( 'Company name', 'theme-my-login' ) ?></label>
		<input type="text" placeholder="Company Name" name="company_name" id="company_name<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'company_name' ); ?>" size="20" tabindex="5" required>
		</p>
		<p>
		<label for="Phone<?php $template->the_instance(); ?>"><?php _e( 'Phone', 'theme-my-login' ) ?></label>
		<input type="text" placeholder="Phone" name="phone" id="phone<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'phone' ); ?>" size="20" tabindex="6" required>
		</p>
		<p>
		<label for="Address<?php $template->the_instance(); ?>"><?php _e( 'Address', 'theme-my-login' ) ?></label>
		<input type="text" placeholder="Address" name="address" id="address<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'address' ); ?>" size="20" tabindex="7" required>
		</p>
		<p>
		<label for="Zip code<?php $template->the_instance(); ?>"><?php _e( 'Zip code', 'theme-my-login' ) ?></label>
		<input type="text" placeholder="Zip Code" name="zip_code" id="zip_code<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'zip_code' ); ?>" size="20" tabindex="8" required>
		</p>
		<p>
		<label for="City<?php $template->the_instance(); ?>"><?php _e( 'City', 'theme-my-login' ) ?></label>
		<input type="text"  placeholder="City" name="city" id="city<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'city' ); ?>" size="20" tabindex="9" required>
		</p>
		<p>
		<label for="Country<?php $template->the_instance(); ?>"><?php _e( 'Country', 'theme-my-login' ) ?></label>
		<select name="country" id="country<?php $template->the_instance(); ?>" value="<?php $template->the_posted_value( 'country' ); ?>" required>
		<option value="Afghanistan">Afghanistan</option>
		<option value="Albania">Albania</option>
		<option value="Algeria">Algeria</option>
		<option value="American Samoa">American Samoa</option>
		<option value="Andorra">Andorra</option>
		<option value="Angola">Angola</option>
		<option value="Anguilla">Anguilla</option>
		<option value="Antartica">Antarctica</option>
		<option value="Antigua and Barbuda">Antigua and Barbuda</option>
		<option value="Argentina">Argentina</option>
		<option value="Armenia">Armenia</option>
		<option value="Aruba">Aruba</option>
		<option value="Australia">Australia</option>
		<option value="Austria">Austria</option>
		<option value="Azerbaijan">Azerbaijan</option>
		<option value="Bahamas">Bahamas</option>
		<option value="Bahrain">Bahrain</option>
		<option value="Bangladesh">Bangladesh</option>
		<option value="Barbados">Barbados</option>
		<option value="Belarus">Belarus</option>
		<option value="Belgium">Belgium</option>
		<option value="Belize">Belize</option>
		<option value="Benin">Benin</option>
		<option value="Bermuda">Bermuda</option>
		<option value="Bhutan">Bhutan</option>
		<option value="Bolivia">Bolivia</option>
		<option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
		<option value="Botswana">Botswana</option>
		<option value="Bouvet Island">Bouvet Island</option>
		<option value="Brazil">Brazil</option>
		<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
		<option value="Brunei Darussalam">Brunei Darussalam</option>
		<option value="Bulgaria">Bulgaria</option>
		<option value="Burkina Faso">Burkina Faso</option>
		<option value="Burundi">Burundi</option>
		<option value="Cambodia">Cambodia</option>
		<option value="Cameroon">Cameroon</option>
		<option value="Canada">Canada</option>
		<option value="Cape Verde">Cape Verde</option>
		<option value="Cayman Islands">Cayman Islands</option>
		<option value="Central African Republic">Central African Republic</option>
		<option value="Chad">Chad</option>
		<option value="Chile">Chile</option>
		<option value="China">China</option>
		<option value="Christmas Island">Christmas Island</option>
		<option value="Cocos Islands">Cocos (Keeling) Islands</option>
		<option value="Colombia">Colombia</option>
		<option value="Comoros">Comoros</option>
		<option value="Congo">Congo</option>
		<option value="Congo">Congo, the Democratic Republic of the</option>
		<option value="Cook Islands">Cook Islands</option>
		<option value="Costa Rica">Costa Rica</option>
		<option value="Cota D'Ivoire">Cote d'Ivoire</option>
		<option value="Croatia">Croatia (Hrvatska)</option>
		<option value="Cuba">Cuba</option>
		<option value="Cyprus">Cyprus</option>
		<option value="Czech Republic">Czech Republic</option>
		<option value="Denmark">Denmark</option>
		<option value="Djibouti">Djibouti</option>
		<option value="Dominica">Dominica</option>
		<option value="Dominican Republic">Dominican Republic</option>
		<option value="East Timor">East Timor</option>
		<option value="Ecuador">Ecuador</option>
		<option value="Egypt">Egypt</option>
		<option value="El Salvador">El Salvador</option>
		<option value="Equatorial Guinea">Equatorial Guinea</option>
		<option value="Eritrea">Eritrea</option>
		<option value="Estonia">Estonia</option>
		<option value="Ethiopia">Ethiopia</option>
		<option value="Falkland Islands">Falkland Islands (Malvinas)</option>
		<option value="Faroe Islands">Faroe Islands</option>
		<option value="Fiji">Fiji</option>
		<option value="Finland">Finland</option>
		<option value="France">France</option>
		<option value="France Metropolitan">France, Metropolitan</option>
		<option value="French Guiana">French Guiana</option>
		<option value="French Polynesia">French Polynesia</option>
		<option value="French Southern Territories">French Southern Territories</option>
		<option value="Gabon">Gabon</option>
		<option value="Gambia">Gambia</option>
		<option value="Georgia">Georgia</option>
		<option value="Germany">Germany</option>
		<option value="Ghana">Ghana</option>
		<option value="Gibraltar">Gibraltar</option>
		<option value="Greece">Greece</option>
		<option value="Greenland">Greenland</option>
		<option value="Grenada">Grenada</option>
		<option value="Guadeloupe">Guadeloupe</option>
		<option value="Guam">Guam</option>
		<option value="Guatemala">Guatemala</option>
		<option value="Guinea">Guinea</option>
		<option value="Guinea-Bissau">Guinea-Bissau</option>
		<option value="Guyana">Guyana</option>
		<option value="Haiti">Haiti</option>
		<option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
		<option value="Holy See">Holy See (Vatican City State)</option>
		<option value="Honduras">Honduras</option>
		<option value="Hong Kong">Hong Kong</option>
		<option value="Hungary">Hungary</option>
		<option value="Iceland">Iceland</option>
		<option value="India">India</option>
		<option value="Indonesia">Indonesia</option>
		<option value="Iran">Iran (Islamic Republic of)</option>
		<option value="Iraq">Iraq</option>
		<option value="Ireland">Ireland</option>
		<option value="Israel">Israel</option>
		<option value="Italy">Italy</option>
		<option value="Jamaica">Jamaica</option>
		<option value="Japan">Japan</option>
		<option value="Jordan">Jordan</option>
		<option value="Kazakhstan">Kazakhstan</option>
		<option value="Kenya">Kenya</option>
		<option value="Kiribati">Kiribati</option>
		<option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
		<option value="Korea">Korea, Republic of</option>
		<option value="Kuwait">Kuwait</option>
		<option value="Kyrgyzstan">Kyrgyzstan</option>
		<option value="Lao">Lao People's Democratic Republic</option>
		<option value="Latvia">Latvia</option>
		<option value="Lebanon" selected>Lebanon</option>
		<option value="Lesotho">Lesotho</option>
		<option value="Liberia">Liberia</option>
		<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
		<option value="Liechtenstein">Liechtenstein</option>
		<option value="Lithuania">Lithuania</option>
		<option value="Luxembourg">Luxembourg</option>
		<option value="Macau">Macau</option>
		<option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
		<option value="Madagascar">Madagascar</option>
		<option value="Malawi">Malawi</option>
		<option value="Malaysia">Malaysia</option>
		<option value="Maldives">Maldives</option>
		<option value="Mali">Mali</option>
		<option value="Malta">Malta</option>
		<option value="Marshall Islands">Marshall Islands</option>
		<option value="Martinique">Martinique</option>
		<option value="Mauritania">Mauritania</option>
		<option value="Mauritius">Mauritius</option>
		<option value="Mayotte">Mayotte</option>
		<option value="Mexico">Mexico</option>
		<option value="Micronesia">Micronesia, Federated States of</option>
		<option value="Moldova">Moldova, Republic of</option>
		<option value="Monaco">Monaco</option>
		<option value="Mongolia">Mongolia</option>
		<option value="Montserrat">Montserrat</option>
		<option value="Morocco">Morocco</option>
		<option value="Mozambique">Mozambique</option>
		<option value="Myanmar">Myanmar</option>
		<option value="Namibia">Namibia</option>
		<option value="Nauru">Nauru</option>
		<option value="Nepal">Nepal</option>
		<option value="Netherlands">Netherlands</option>
		<option value="Netherlands Antilles">Netherlands Antilles</option>
		<option value="New Caledonia">New Caledonia</option>
		<option value="New Zealand">New Zealand</option>
		<option value="Nicaragua">Nicaragua</option>
		<option value="Niger">Niger</option>
		<option value="Nigeria">Nigeria</option>
		<option value="Niue">Niue</option>
		<option value="Norfolk Island">Norfolk Island</option>
		<option value="Northern Mariana Islands">Northern Mariana Islands</option>
		<option value="Norway">Norway</option>
		<option value="Oman">Oman</option>
		<option value="Pakistan">Pakistan</option>
		<option value="Palau">Palau</option>
		<option value="Panama">Panama</option>
		<option value="Papua New Guinea">Papua New Guinea</option>
		<option value="Paraguay">Paraguay</option>
		<option value="Peru">Peru</option>
		<option value="Philippines">Philippines</option>
		<option value="Pitcairn">Pitcairn</option>
		<option value="Poland">Poland</option>
		<option value="Portugal">Portugal</option>
		<option value="Puerto Rico">Puerto Rico</option>
		<option value="Qatar">Qatar</option>
		<option value="Reunion">Reunion</option>
		<option value="Romania">Romania</option>
		<option value="Russia">Russian Federation</option>
		<option value="Rwanda">Rwanda</option>
		<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
		<option value="Saint LUCIA">Saint LUCIA</option>
		<option value="Saint Vincent">Saint Vincent and the Grenadines</option>
		<option value="Samoa">Samoa</option>
		<option value="San Marino">San Marino</option>
		<option value="Sao Tome and Principe">Sao Tome and Principe</option> 
		<option value="Saudi Arabia">Saudi Arabia</option>
		<option value="Senegal">Senegal</option>
		<option value="Seychelles">Seychelles</option>
		<option value="Sierra">Sierra Leone</option>
		<option value="Singapore">Singapore</option>
		<option value="Slovakia">Slovakia (Slovak Republic)</option>
		<option value="Slovenia">Slovenia</option>
		<option value="Solomon Islands">Solomon Islands</option>
		<option value="Somalia">Somalia</option>
		<option value="South Africa">South Africa</option>
		<option value="South Georgia">South Georgia and the South Sandwich Islands</option>
		<option value="Span">Spain</option>
		<option value="SriLanka">Sri Lanka</option>
		<option value="St. Helena">St. Helena</option>
		<option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
		<option value="Sudan">Sudan</option>
		<option value="Suriname">Suriname</option>
		<option value="Svalbard">Svalbard and Jan Mayen Islands</option>
		<option value="Swaziland">Swaziland</option>
		<option value="Sweden">Sweden</option>
		<option value="Switzerland">Switzerland</option>
		<option value="Syria">Syrian Arab Republic</option>
		<option value="Taiwan">Taiwan, Province of China</option>
		<option value="Tajikistan">Tajikistan</option>
		<option value="Tanzania">Tanzania, United Republic of</option>
		<option value="Thailand">Thailand</option>
		<option value="Togo">Togo</option>
		<option value="Tokelau">Tokelau</option>
		<option value="Tonga">Tonga</option>
		<option value="Trinidad and Tobago">Trinidad and Tobago</option>
		<option value="Tunisia">Tunisia</option>
		<option value="Turkey">Turkey</option>
		<option value="Turkmenistan">Turkmenistan</option>
		<option value="Turks and Caicos">Turks and Caicos Islands</option>
		<option value="Tuvalu">Tuvalu</option>
		<option value="Uganda">Uganda</option>
		<option value="Ukraine">Ukraine</option>
		<option value="United Arab Emirates">United Arab Emirates</option>
		<option value="United Kingdom">United Kingdom</option>
		<option value="United States">United States</option>
		<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
		<option value="Uruguay">Uruguay</option>
		<option value="Uzbekistan">Uzbekistan</option>
		<option value="Vanuatu">Vanuatu</option>
		<option value="Venezuela">Venezuela</option>
		<option value="Vietnam">Viet Nam</option>
		<option value="Virgin Islands (British)">Virgin Islands (British)</option>
		<option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
		<option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
		<option value="Western Sahara">Western Sahara</option>
		<option value="Yemen">Yemen</option>
		<option value="Yugoslavia">Yugoslavia</option>
		<option value="Zambia">Zambia</option>
		<option value="Zimbabwe">Zimbabwe</option>
		</select>
        </p>
		<p class="become_expert_txt"><input type="checkbox" name="become_expert" id="become_expert<?php $template->the_instance(); ?>" class="input become_expert" value="<?php $template->the_posted_value( 'become_expert' ); ?>"/><span class="label_form"></span>Become an <span>Expert</span><br><p class="expert_short_text">Lorem ipsum dolor sit amet, vocibus patrioque consetetur vim ad. Aeque nihil accusamus id vix. Cibo tempor elaboraret eam eu.</p></p>
		<p>
		<?php
		global $wp_roles, $wpdb;
		$roles = $wp_roles->get_names();  
		?>
		</p>
		<p class="answer_2">
		<label><?php _e( 'Select Role', 'theme-my-login' ) ?></label>
		<?php
		foreach($query as $q){ ?>
		<span class="footet_txt"><input type="radio" name="mem_levels" value="<?php echo $q->id; ?>" required><?php echo $q->level_name; ?></span>
		<?php } ?>
		</p>
		<p class="answer">
		<?php
		$table_name = $wpdb->prefix . "membership_levels";
		$myrows= $wpdb->get_results("SELECT * from $table_name where id IN (5,6,7)");
		foreach($myrows as $row){ ?>
		<span class="footet_txt"><input type="radio" name="mem_levels" value="<?php echo $row->id; ?>" required><?php echo $row->level_name; ?></span>
		<?php } ?>
		</p>
		<p class="tml-submit-wrap">
		<input type="submit" name="wp-submit" id="wp-submit<?php $template->the_instance(); ?>" value="<?php esc_attr_e( 'Register', 'theme-my-login' ); ?>" />
		<input type="hidden" name="redirect_to" value="<?php $template->the_redirect_url( 'register' ); ?>" />
		<input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
		<input type="hidden" name="action" value="register" />
		</p>
		<!--<div class="sign_up_btn"><a href="#">SIGN up</a></div>-->
	</form>
	<?php $template->the_action_links( array( 'SIGN up' => false ) ); ?>
</div>
<script>
jQuery(document).ready(function(){
	jQuery("#pass1").attr("placeholder", "Password");
	jQuery("#pass2").attr("placeholder", "Confirm Password");
	jQuery(function(){
		 jQuery(".answer").hide();
			jQuery(".become_expert").click(function(){
				if(jQuery(this).is(":checked")){
					jQuery(".answer").show();
					jQuery(".answer_2").hide();
				}else{
					jQuery(".answer").hide();
					jQuery(".answer_2").show();
				}
			});
		});
    }); 
jQuery(document).ready(function(){
	jQuery("span.footet_txt input").click(function(){
		jQuery("span.footet_txt").removeClass("active");
		jQuery(this).parents("span.footet_txt").addClass("active");
	});
	jQuery("span.footet_txt").click(function(){
		jQuery("span.footet_txt").removeClass("active");
		 jQuery('input[name="mem_levels"]', this).prop("checked",true);
		 //var mem_lev = jQuery('input[name="mem_levels"]', this).val();
		 //alert(mem_lev);
		jQuery(this).addClass("active");
	});
});	
</script>