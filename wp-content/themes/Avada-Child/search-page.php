<?php
/**
 * Template name: Search Page
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); 
if (is_user_logged_in()){
?>

<?php 


	$input_search = $_GET['s'] != '' ? $_GET['s'] : '';
	$select_search = $_GET['select_search'] != '' ? $_GET['select_search'] : '';


	$searchKeyword = $input_search."%";

	if($select_search=='buyers'){

	}else if($select_search=='sellers'){

	}else if($select_search=='events'){

		$table_name = $wpdb->prefix . "event_data";
		$query = $wpdb->get_results("select * from $table_name WHERE eventname LIKE '$searchKeyword'");

		?>
		
		<div class="text-center">
    <div class="a_container">
        <div class="container">
        	<?php if($query){ ?>

        	
            <div class="a_header">
                <?php _e("<h3><strong>Search Results for: ".get_query_var('s')."</strong></h3>"); ?>
            </div>
            <div id="products" class="row list-group event-list">
                <div class="show-more">
                    <?php foreach($query as $event) { 
                    $image = explode("%%",$event->uploadimage);
                    //print_r($image);
                    ?>
                    <div class="item  col-xs-4 col-lg-12 list-group-item">
                        <div class="thumbnail">
                            <div class="event-date">
                                <?php $date=date_create($event->date); ?>
                                <h5 class="month"><?php echo date_format($date,"M"); ?></h5>
                                <hr>
                                <h4 class="date"><?php echo date_format($date,"d"); ?></h4>
                            </div>
                            <img class="group list-group-image item-image" src="<?php foreach($image as $img){if($img != ''){echo $img;}else{echo '';}} ?>" alt="" />
                            <div class="caption">
                                <div class="row">
                                    <div class="grid-fix col-md-12">
                                        <a href="<?php echo site_url() . '/event/' . $event->id; ?>"><h4 class="group inner list-group-item-heading"><?php echo $event->eventname; ?></h4></a>
                                        <div class="event-sched">
                                            <p><span class="glyphicon glyphicon-map-marker"></span><?php echo $event->address; ?></p>
                                            <p><span class="glyphicon glyphicon-time"></span><?php echo $event->time; ?></p>
                                        </div>
                                    </div>
                                    <div class="grid-fix col-xs-12 col-md-12 btn-pos">
                                        <div class="event-overview">
                                             <?php 
                                             $meta = get_user_meta($event->userid, 'company_name', TRUE );
                                             ?>
                                            <p><label>Company:</label><?php if($meta){echo $meta;}else{ echo '-'; }?></p>
                                            <p><label>Event type:</label><?php echo $event->topic; ?></p>
                                            <p><label>Topic:</label><?php echo $event->topic; ?></p>
                                        </div>
                                    </div>
                                    <div class="grid-fix col-xs-12 col-md-12">
									<?php  $url = site_url(); ?>
                                        <div  id="share" class="event-share-btns">
                                            <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo site_url() . '/event/' . $event->id; ?>&title=<?php echo $event->eventname; ?>&source=LinkedIn" class="social"><img src="<?php echo EVENT_URL;?>image/linkedin-dark/linkedin-dark.png" srcset="<?php echo EVENT_URL;?>image/linkedin-dark/linkedin-dark@2x.png 2x,<?php echo EVENT_URL;?>image/linkedin-dark/linkedin-dark@3x.png 3x" class="linkedin-dark"></a>
                                            <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>/event/<?php echo $query[0]->id; ?>" class="social"><img src="<?php echo EVENT_URL;?>image/facebook-dark/facebook-dark.png" srcset="<?php echo EVENT_URL;?>image/facebook-dark/facebook-dark@2x.png 2x,<?php echo EVENT_URL;?>image/facebook-dark/facebook-dark@3x.png 3x" class="facebook-dark"></a>
                                            <a href="https://plus.google.com/share?url=<?php echo $url; ?>/event/<?php echo $query[0]->id; ?>" class="social"><img src="<?php echo EVENT_URL;?>image/gplus-dark/gplus-dark.png" srcset="<?php echo EVENT_URL;?>image/gplus-dark/gplus-dark@2x.png 2x,<?php echo EVENT_URL;?>image/gplus-dark/gplus-dark@3x.png 3x" class="gplus-dark"></a>
                                            <a href="https://www.xing.com/spi/shares/new?url=<?php echo $url; ?>/event/<?php echo $query[0]->id; ?>" class="social"><img src="<?php echo EVENT_URL;?>image/xing-dark/xing-dark.png" srcset="<?php echo EVENT_URL;?>image/xing-dark/xing-dark@2x.png 2x,<?php echo EVENT_URL;?>image/xing-dark/xing-dark@3x.png 3x" class="xing-dark"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   <?php } ?>
             </div>
            </div>
            <div class="a_footer">
                <p class="text-center"><a id="next" href="#"><strong>SHOW MORE</strong></a> </p>
            </div>
            <?php }else{ ?>
            		 <div class="a_footer">
                		<p class="text-center"><strong>No Records found from this Event category</strong></a> </p>
            	</div>
           <?php  } ?>
        </div>
    </div>
</div>


<?php
		

	}else if($select_search=='literature'){

		$table_name = $wpdb->prefix . "literature";
		$query = $wpdb->get_results("select * from $table_name WHERE property_name LIKE '$searchKeyword'");

	}else if($select_search=='post'){

		//$table_name = $wpdb->prefix . "posts";
		//$table_name1 = $wpdb->prefix . "postmeta";

		//echo $sql= "select * from $table_name join $table_name1 ON $table_name.ID = $table_name1.post_id WHERE $table_name.post_title LIKE '$searchKeyword'";
		
		//$query = $wpdb->get_results("select * from $table_name WHERE post_title LIKE '$searchKeyword'");
		//pr($query);

				$s=get_search_query();
				$args = array(
                		's' =>$s
            		);
				$the_query = new WP_Query( $args );

				if ( $the_query->have_posts() ) {

        			_e("<h2 style='font-weight:bold;color:#000'>Search Results for: ".get_query_var('s')."</h2>");
        			while ( $the_query->have_posts() ) {
           					$the_query->the_post();
                ?>
	                    <li>
	                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	                    </li>
                <?php
        			}

    			}else{
				?>
        		<h2 style='font-weight:bold;color:#000'>Nothing Found</h2>
        		<div class="alert alert-info">
          			<p>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</p>
        		</div>
        <?php }


	}else if($select_search=='templates'){


	}else if($select_search=='drafts'){


	}else if($select_search=='descriptions'){


	}else{
		
	}
	

	
	


//}


?>

<?php
}
?>

<?php do_action( 'avada_after_content' ); 

get_footer();
?>