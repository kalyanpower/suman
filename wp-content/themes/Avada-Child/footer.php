<?php
/**
 * The footer template.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
					<?php do_action( 'avada_after_main_content' ); ?>

				</div>  <!-- fusion-row -->
			</main>  <!-- #main -->
			<?php do_action( 'avada_after_main_container' ); ?>

			<?php global $social_icons; ?>

			<?php
			/**
			 * Get the correct page ID.
			 */
			$c_page_id = Avada()->fusion_library->get_page_id();
			?>

			<?php
			/**
			 * Only include the footer.
			 */
			?>
			<?php if ( ! is_page_template( 'blank.php' ) ) : ?>
				<?php $footer_parallax_class = ( 'footer_parallax_effect' === Avada()->settings->get( 'footer_special_effects' ) ) ? ' fusion-footer-parallax' : ''; ?>

				<div class="fusion-footer<?php echo esc_attr( $footer_parallax_class ); ?>">
					<?php get_template_part( 'templates/footer-content' ); ?>
				</div> <!-- fusion-footer -->
			<?php endif; // End is not blank page check. ?>

			<?php
			/**
			 * Add sliding bar.
			 */
			?>
			<?php if ( Avada()->settings->get( 'slidingbar_widgets' ) && ! is_page_template( 'blank.php' ) ) : ?>
				<?php get_template_part( 'sliding_bar' ); ?>
			<?php endif; ?>
		</div> <!-- wrapper -->

		<?php
		/**
		 * Check if boxed side header layout is used; if so close the #boxed-wrapper container.
		 */
		$page_bg_layout = ( $c_page_id ) ? get_post_meta( $c_page_id, 'pyre_page_bg_layout', true ) : 'default';
		?>
		<?php if ( ( ( 'Boxed' === Avada()->settings->get( 'layout' ) && 'default' === $page_bg_layout ) || 'boxed' === $page_bg_layout ) && 'Top' !== Avada()->settings->get( 'header_position' ) ) : ?>
			</div> <!-- #boxed-wrapper -->
		<?php endif; ?>
		<?php if ( ( ( 'Boxed' === Avada()->settings->get( 'layout' ) && 'default' === $page_bg_layout ) || 'boxed' === $page_bg_layout ) && 'framed' === Avada()->settings->get( 'scroll_offset' ) && 0 !== intval( Avada()->settings->get( 'margin_offset', 'top' ) ) ) : ?>
			<div class="fusion-top-frame"></div>
			<div class="fusion-bottom-frame"></div>
			<?php if ( 'None' !== Avada()->settings->get( 'boxed_modal_shadow' ) ) : ?>
				<div class="fusion-boxed-shadow"></div>
			<?php endif; ?>
		<?php endif; ?>
		<a class="fusion-one-page-text-link fusion-page-load-link"></a>

		<?php wp_footer(); ?>
		<script>
//square offer fill icons
jQuery("#companyoffer").hover(

    function () {
    jQuery("#companyoffer img").attr("src","<?php echo get_stylesheet_directory_uri(); ?>/images/company-fill/company-fill.png")
        .attr("srcset","<?php echo get_stylesheet_directory_uri(); ?>/images/company-fill/company-fill@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/company-fill/company-fill@3x.png 3x");
    },
    function () {
        jQuery("#companyoffer img").attr("src","<?php echo get_stylesheet_directory_uri(); ?>/images/company/company-stroke.png")
            .attr("srcset","<?php echo get_stylesheet_directory_uri(); ?>/images/company/company-stroke@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/company/company-stroke@3x.png 3x");
    });
jQuery("#expertoffer").hover(
    function () {
        jQuery("#expertoffer img").attr("src","<?php echo get_stylesheet_directory_uri(); ?>/images/expert-fill/expert-fill.png")
            .attr("srcset","<?php echo get_stylesheet_directory_uri(); ?>/images/expert-fill/expert-fill@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/expert-fill/expert-fill@3x.png 3x");
    },
    function () {
        jQuery("#expertoffer img").attr("src","<?php echo get_stylesheet_directory_uri(); ?>/images/expert-stroke/expert-stroke.png")
            .attr("srcset","<?php echo get_stylesheet_directory_uri(); ?>/images/expert-stroke/expert-stroke@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/expert-stroke/expert-stroke@3x.png 3x");
    });
jQuery("#buyeroffer").hover(
    function () {
        jQuery("#buyeroffer img").attr("src","<?php echo get_stylesheet_directory_uri(); ?>/images/buyer-fill/buyer-fill.png")
            .attr("srcset","<?php echo get_stylesheet_directory_uri(); ?>/images/buyer-fill/buyer-fill@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/buyer-fill/buyer-fill@3x.png 3x");
    },
    function () {
        jQuery("#buyeroffer img").attr("src","<?php echo get_stylesheet_directory_uri(); ?>/images/buyer-stroke/buyer-stroke.png")
            .attr("srcset","<?php echo get_stylesheet_directory_uri(); ?>/images/buyer-stroke/buyer-stroke@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/buyer-stroke/buyer-stroke@3x.png 3x");
    });

	
//max characters to show in square offer description
jQuery(function(){
    jQuery(".sqare-offer h6").each(function(i){
        len=jQuery(this).text().length;
        if(len>80)
        {
            jQuery(this).text(jQuery(this).text().substr(0,115));
        }
    });
});

//grid list
$('#list').click(function(event){
		
        event.preventDefault();
        jQuery('#list img').attr("src","<?php echo get_stylesheet_directory_uri(); ?>/images/list-view-selected/list-view-selected.png")
            .attr("srcset","<?php echo get_stylesheet_directory_uri(); ?>/images/list-view-selected/list-view-selected@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/list-view-selected/list-view-selected@3x.png 3x");
        jQuery('#grid img').attr("src","<?php echo get_stylesheet_directory_uri(); ?>/images/grid-view/grid-view.png")
        .attr("srcset","<?php echo get_stylesheet_directory_uri(); ?>/images/grid-view/grid-view@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/grid-view/grid-view@3x.png 3x");
        jQuery('#products .item').removeClass('grid-group-item');
        jQuery('#products .item').addClass('list-group-item');
        jQuery('#products .grid-fix').addClass('col-md-6');
});

jQuery('#grid').click(function(event){

        event.preventDefault();
			//alert('sdfsdf');
        jQuery('#list img').attr("src","<?php echo get_stylesheet_directory_uri(); ?>/images/list-view/list-view.png")
        .attr("srcset","<?php echo get_stylesheet_directory_uri(); ?>/images/list-view/list-view@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/list-view/list-view@3x.png 3x");
        jQuery('#grid img').attr("src","<?php echo get_stylesheet_directory_uri(); ?>/images/grid-view-selected/grid-view-selected.png")
        .attr("srcset","<?php echo get_stylesheet_directory_uri(); ?>/images/grid-view-selected/grid-view-selected@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/grid-view-selected/grid-view-selected@3x.png 3x");
        jQuery('#products .item').removeClass('list-group-item');
        jQuery('#products .item').addClass('grid-group-item');
        jQuery('#products .grid-fix').removeClass('col-md-6');
        jQuery('#products .grid-fix').addClass('col-md-12');
});

//list item icon change on mouse over
jQuery('.loc-hov').hover(
    function () {
        jQuery('.loc-hov').children().attr("src","<?php echo get_stylesheet_directory_uri(); ?>/images/location-hover/location-hover.png")
            .attr("srcset","<?php echo get_stylesheet_directory_uri(); ?>/images/location-hover/location-hover@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/location-hover/location-hover@3x.png 3x");
    },
    function () {
        jQuery('.loc-hov').children().attr("src","<?php echo get_stylesheet_directory_uri(); ?>/images/location/location.png")
            .attr("srcset","<?php echo get_stylesheet_directory_uri(); ?>/images/location/location@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/location/location@3x.png 3x");
    });

jQuery('.cat-hov').hover(
    function () {
        jQuery('.cat-hov').children().attr("src","<?php echo get_stylesheet_directory_uri(); ?>/images/category-hover/category-hover.png")
            .attr("srcset","<?php echo get_stylesheet_directory_uri(); ?>/images/category-hover/category-hover@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/category-hover/category-hover@3x.png 3x");
    },
    function () {
        jQuery('.cat-hov').children().attr("src","<?php echo get_stylesheet_directory_uri(); ?>/images/category/category.png")
            .attr("srcset","<?php echo get_stylesheet_directory_uri(); ?>/images/category/category@2x.png 2x,<?php echo get_stylesheet_directory_uri(); ?>/images/category/category@3x.png 3x");
    });

//show more functionality
jQuery(document).ready(function(){

    var list = jQuery(".list-group .show-more .item");
    var numToShow = 3;
    var button = jQuery("#next");
    var numInList = list.length;
    list.hide();
    if (numInList > numToShow) {
        button.show();
    }
    list.slice(0, numToShow).show();

    button.click(function(event){
        event.preventDefault();
        var showing = list.filter(':visible').length;
        list.slice(showing - 1, showing + numToShow).fadeIn();
        var nowShowing = list.filter(':visible').length;
        if (nowShowing >= numInList) {
            button.hide();
        }
    });

});

//max characters to show in info box description
jQuery(function(){
    jQuery(".info-box-description").each(function(i){
        len=jQuery(this).text().length;
        if(len>80)
        {
            jQuery(this).text(jQuery(this).text().substr(0,250));
        }
    });
});

</script>
<script>
jQuery(function(){
    jQuery(".project-back p").each(function(i){
        document.querySelector('p').classList.toggle('ow');
    }, false);
});

jQuery(function(){
    jQuery(".project-front p").each(function(i){
        document.querySelector('p').classList.toggle('ow');
    }, false);
});

String.prototype.filename=function(extension){
    var s= this.replace(/\\/g, '/');
    s= s.substring(s.lastIndexOf('/')+ 1);
    return extension? s.replace(/[?#].+$/, ''): s.split('.')[0];
}

jQuery(function() {
// OPACITY OF BUTTON SET TO 0%
    jQuery(".roll").css("opacity","0");
    jQuery('.image-slider .slick-prev').css("opacity","0");
    jQuery('.image-slider .slick-next').css("opacity","0");

// ON MOUSE OVER

    jQuery(".roll").hover(function () {

// SET OPACITY TO 70%
            jQuery(this).stop().animate({
                opacity: .7
            }, "fast");
            jQuery('.image-slider .slick-prev').stop().animate({
                opacity: .7
            }, "fast");
            jQuery('.image-slider .slick-next').stop().animate({
                opacity: .7
            }, "fast");
        },


// ON MOUSE OUT
        function () {

// SET OPACITY BACK TO 50%
            jQuery(this).stop().animate({
                opacity: 0
            }, "slow");
            jQuery('.image-slider .slick-prev').stop().animate({
                opacity: 0
            }, "slow");
            jQuery('.image-slider .slick-next').stop().animate({
                opacity: 0
            }, "slow");
        });
});

$(document).ready(function() {
    $('.image-slider').not('.slick-initialized').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        infinite: false,
        arrows: true,
        asNavFor: '.slider-nav-thumbnails'
    });
    jQuery('.sd-slider').not('.slick-initialized').slick({
        lazyLoad: 'ondemand',
        slidesToShow: 7,
        slidesToScroll: 1
    });
    jQuery('.slider').not('.slick-initialized').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: false,
        asNavFor: '.slider-nav-thumbnails',
    });

    jQuery('.slider-nav-thumbnails').not('.slick-initialized').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider',
        dots: false,
        focusOnSelect: false,
        arrows: false,
        infinite: false,
        accessibility: false
    });

//remove active class from all thumbnail slides
    jQuery('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');

//set active class to first thumbnail slides
    jQuery('.slider-nav-thumbnails .slick-slide').eq(0).addClass('slick-active');

// On before slide change match active thumbnail to current slide
    jQuery('.slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var mySlideNumber = nextSlide;
        jQuery('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');
        jQuery('.slider-nav-thumbnails .slick-slide').eq(mySlideNumber).addClass('slick-active');
    });
    jQuery('.slider-nav-thumbnails .slick-slide').on('click', function (event) {
        jQuery('.slider').slick('slickGoTo', jQuery(this).data('slickIndex'));
    });

//UPDATED

    jQuery('.slider').on('afterChange', function(event, slick, currentSlide){
        jQuery('.content').hide();
        jQuery('.content[data-id=' + (currentSlide + 1) + ']').show();
    });

    jQuery(document).ready(function() {
        jQuery(".project-detail").not('.slick-initialized').slick({
            slidesToShow: 1,
            arrows: true,
            asNavFor: '.project-strip',
            autoplay: true,
            autoplaySpeed: 3000,
            dots: true
        });

        jQuery(".project-strip").not('.slick-initialized').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: false,
            asNavFor: '.project-detail',
            dots: false,
            infinite: true,
            centerMode: false,
            focusOnSelect: true
        });
        jQuery('.sd-slider').not('.slick-initialized').slick({
            lazyLoad: 'ondemand',
            slidesToShow: 7,
            slidesToScroll: 1
        });
    });
});
  jQuery(document).ready(function() {
    var IDs = [];
    jQuery(".sd-slider").find("img").each(function(){ IDs.push(this.id); });
    for (var i=0; i < IDs.length; i++) {
        IDs[i] = ("#").concat(IDs[i]);
    }

    jQuery(IDs.join(", ")).hover(
        function () {
            jQuery(this).attr("src", ("http://112.196.9.211:8888/companymarekt.ch/wp-content/themes/Avada-Child/images/logo/hover/").concat(jQuery(this).attr('src').filename()).concat(".png"));
        }
        ,
        function () {
            jQuery(this).attr("src", ("http://112.196.9.211:8888/companymarekt.ch/wp-content/themes/Avada-Child//images/logo/").concat(jQuery(this).attr('src').filename()).concat(".png"));
        }
    );
    var f = 0;
    jQuery("#sd-button").click(
        function () {
            if (f == 0) {
                jQuery('.sd-container').slideUp("slow", function () {
                    jQuery('.sd-container').hide();
                });
                jQuery("#sd-button").html("⌄").css("color","#fff");
                f = 1;
            } else {
                jQuery('.sd-container').slideDown("slow", function () {
                    jQuery('.sd-container').show();
                });
                jQuery("#sd-button").html("^").css("color","#fff");
                f = 0;
            }
        }
    );
});  
jQuery("#es_txt_email_pg").attr("placeholder", "Email");
</script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#search_box_form').click(function() {
            jQuery('.search_box').slideToggle("fast");
        });
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery(".advanced_search_form").click(function(){

           if(jQuery(this).text()==='SHOW FILTERS'){
                jQuery(this).text("HIDE FILTERS");
                jQuery('.advanced_fiter_form').show();
           }else{
                jQuery(this).text("SHOW FILTERS");
                jQuery('.advanced_fiter_form').hide();
           }
        });
    });
</script>

<?php

  $table_name = $wpdb->prefix . "advertisement";
    $query_max = $wpdb->get_var("SELECT MAX(selling_price) FROM $table_name");

 

 ?>
<script>
$(function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: <?php echo $query_max; ?>,
            values: [ 15000, 40000 ],
            slide: function( event, ui ) {
                
                var offset1 = $(this).children('.ui-slider-handle').first().offset();
                var offset2 = $(this).children('.ui-slider-handle').last().offset();
                $(".tooltip1").css('top',offset1.top).css('left',offset1.left).show();
                $(".tooltip2").css('top',offset2.top).css('left',offset2.left).show();
                         
                $('#tooltip1').html(ui.values[ 0 ] + '&nbsp;CHF');
                $('#tooltip2').html(ui.values[ 1 ] + '&nbsp;CHF');
                $('#amount1').val(ui.values[ 0 ]);
                $('#amount2').val(ui.values[ 1 ]);
                
            },
            stop:function(event,ui){
                $(".tooltip").hide();
            }
        });
    
    $('#amount').change(function(){
        $( "#slider-range" ).slider( "values", 0, $('#amount1').val()  );
    });
    $('#amount').change(function(){
        $( "#slider-range" ).slider( "values", 1, $('#amount2').val()  );
    });
        
    });
  </script>
  <script type="text/javascript">
    function getval(sel){
        //alert(sel.value);
        jQuery("#ads_type").val(sel.value);
    }
  </script>
</body>
</html>
