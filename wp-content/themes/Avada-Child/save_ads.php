<?php
/**
 * Template name: Save Ads
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<style>
.like_color{
	background-color:#2196F3;
	color:#fff;
}
</style>

<?php
get_header(); 

if(is_user_logged_in()){ 
global $wpdb;
$id = get_current_user_id();
$table_name = $wpdb->prefix . "advertisement"; 
$query= $wpdb->get_results("SELECT * from $table_name");

/*
  foreach ($query as $queryval) {
	  $queryvall = $queryval->id;
  }
$query_like = $wpdb->get_results("SELECT * from wp_like_status where user_id ='$id' AND like_status ='1' And ads_id=$queryvall ");
 foreach ($query_like as $query_lik) {
	  $query_li = $query_lik->id;
  }
*/
?>
<div class="double-grid">
<div class="row">
			<div class="col-md-8">
			<div class="save_adds button_same_class"><a href="<?php echo site_url(); ?>/show-save-adds/"> <input type="button" value="Show Save Adds"  class="save_add" /></a></div>
            <?php 
			
            if($query){
            foreach ($query as $queryvalue) {
              $img_src = array_filter(explode("%%",$queryvalue->upload_image));  
              ?>
                <div class="col-md-6 double_middle">
                    <div class="thumbnail">
                        <img class="group list-group-image item-image" src="<?php echo $img_src[0]; ?>" alt="featured image1" />
                        <img src="<?php echo ADS_URL; ?>/images/add-to-favorites/add-to-favorites.png" srcset="<?php echo ADS_URL; ?>/images/add-to-favorites/add-to-favorites@2x.png 2x,<?php echo ADS_URL; ?>/images/add-to-favorites/add-to-favorites@3x.png 3x" class="add_to_favorites">
                        <div class="caption">
                                <div class="txt_item">
                                    <a href="#"><h4 class="group inner list-group-item-heading"><?php echo '<a href="'. site_url() .'/property/?ad=' . $queryvalue->property_name . '">' . $queryvalue->property_name . '</a>'; ?></h4></a>
                                    <p class="group inner list-group-item-text"><?php echo $queryvalue->selling_price; ?> CHF</p>
                                </div>
                                <div class="grid-fix btn-pos">
                                    <a class="btn a-btn cat-hov" href="#"><img src="<?php echo ADS_URL; ?>/images/category/category.png" srcset="<?php echo ADS_URL; ?>/images/category/category@2x.png 2x,<?php echo ADS_URL; ?>/images/category/category@3x.png 3x" class="category-hover"><?php echo $queryvalue->industry; ?></a>
                                    <a class="btn a-btn loc-hov" href="#"><img src="<?php echo ADS_URL; ?>/images/location/location.png" srcset="<?php echo ADS_URL; ?>/images/location/location@2x.png 2x,<?php echo ADS_URL; ?>/images/location/location@3x.png 3x" class="location-hover"><?php echo $queryvalue->country; ?></a>
                                </div>
                                <div class="grid-fix col-xs-12 col-md-12 item-footer-status">
                                    <p><strong>Employees</strong><?php echo $queryvalue->employees; ?></p>
                                    <p><strong>Turnover</strong><?php echo $queryvalue->turnover; ?></p>
									
									<input type="hidden" name="hidden" class="auto_id" value="<?php echo $queryvalue->id; ?>" />
									<input type="hidden" name="hidden" class="user_idd" value="<?php echo $id; ?>" />
									
									<input type="hidden" name="myfield" class="like_input" value="0" size="5" readonly="readonly" data-id="<?php echo $queryvalue->id; ?>"><br>
									<input type="button" value="Like! "  class="like"  /> 
									<input type="button" value="Unlike!"  class="unlike" />
                                </div>

                           
                        </div>
                    </div>
            </div>
            <?php } } ?>
               </div>
              <div class="col-md-4"></div>
            </div>
            </div>
<?php }
else{
echo '<h2 class="loggedout_msg">You have to logged in to view this page</h2>';
}
?>
	
<?php do_action( 'avada_after_content' ); 

get_footer();
?>
<script type="text/javascript">



jQuery('.like').click(function(){
	jQuery(this).closest('.item-footer-status').find('.like').addClass('like_color');
	
  jQuery(this).closest('.item-footer-status').find('.like_input').val('1');
 var like = $(this).closest('.item-footer-status').find('.like_input').val();
 if(like == 1){
	 jQuery(this).closest('.item-footer-status').find('.like').addClass('like_color');
 }
var auto_id = jQuery(this).closest('.item-footer-status').find('.auto_id').val();
var user_idd = jQuery(this).closest('.item-footer-status').find('.user_idd').val();
  $.ajax({
           type: "POST",
           url: '<?php echo admin_url( 'admin-ajax.php' );?>',
           data: {auto_id: auto_id, like: like, user_idd: user_idd, action:'update_save_adds'},
           success: function(data)
           {
              // alert(data); // show response from the php script.
           }
         });

	 
		
		});



jQuery('.unlike').click(function(){
	jQuery(this).closest('.item-footer-status').find('.like').removeClass('like_color');	removeClass
 jQuery(this).closest('.item-footer-status').find('.like_input').val('0');
 var like = $(this).closest('.item-footer-status').find('.like_input').val();
var auto_id = jQuery(this).closest('.item-footer-status').find('.auto_id').val();
var user_idd = jQuery(this).closest('.item-footer-status').find('.user_idd').val();
  $.ajax({
           type: "POST",
           url: '<?php echo admin_url( 'admin-ajax.php' );?>',
           data: {auto_id: auto_id, like: like, user_idd: user_idd, action:'update_save_adds'},
           success: function(data)
           {
              // alert(data); // show response from the php script.
           }
         });
		});

</script>