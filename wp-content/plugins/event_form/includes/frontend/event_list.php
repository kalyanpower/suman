<?php
global $wpdb;
$table_name = $wpdb->prefix . "event_data"; 
$query= $wpdb->get_results("SELECT * from $table_name");
?>
<div class="text-center">
    <div class="a_container">
        <div class="container">
            <div class="a_header">
                <h3><strong>ALL EVENTS</strong></h3>
            </div>
            <div id="products" class="row list-group event-list">
                <div class="show-more">
                    <?php foreach($query as $event) { 
                    $image = explode("%%",$event->uploadimage);
                    //print_r($image);
                    ?>
                    <div class="item  col-xs-4 col-lg-12 list-group-item">
                        <div class="thumbnail">
                            <div class="event-date">
                                <?php $date=date_create($event->date); ?>
                                <h5 class="month"><?php echo date_format($date,"M"); ?></h5>
                                <hr>
                                <h4 class="date"><?php echo date_format($date,"d"); ?></h4>
                            </div>
                            <img class="group list-group-image item-image" src="<?php foreach($image as $img){if($img != ''){echo $img;}else{echo '';}} ?>" alt="" />
                            <div class="caption">
                                <div class="row">
                                    <div class="grid-fix col-md-12">
                                        <a href="<?php echo site_url() . '/event/' . $event->id; ?>"><h4 class="group inner list-group-item-heading"><?php echo $event->eventname; ?></h4></a>
                                        <div class="event-sched">
                                            <p><span class="glyphicon glyphicon-map-marker"></span><?php echo $event->address; ?></p>
                                            <p><span class="glyphicon glyphicon-time"></span><?php echo $event->time; ?></p>
                                        </div>
                                    </div>
                                    <div class="grid-fix col-xs-12 col-md-12 btn-pos">
                                        <div class="event-overview">
                                             <?php 
                                             $meta = get_user_meta($event->userid, 'company_name', TRUE );
                                             ?>
                                            <p><label>Company:</label><?php if($meta){echo $meta;}else{ echo '-'; }?></p>
                                            <p><label>Event type:</label><?php echo $event->topic; ?></p>
                                            <p><label>Topic:</label><?php echo $event->topic; ?></p>
                                        </div>
                                    </div>
                                    <div class="grid-fix col-xs-12 col-md-12">
									<?php  $url = site_url(); ?>
                                        <div  id="share" class="event-share-btns">
                                            <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo site_url() . '/event/' . $event->id; ?>&title=<?php echo $event->eventname; ?>&source=LinkedIn" class="social"><img src="<?php echo EVENT_URL;?>image/linkedin-dark/linkedin-dark.png" srcset="<?php echo EVENT_URL;?>image/linkedin-dark/linkedin-dark@2x.png 2x,<?php echo EVENT_URL;?>image/linkedin-dark/linkedin-dark@3x.png 3x" class="linkedin-dark"></a>
                                            <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>/event/<?php echo $query[0]->id; ?>" class="social"><img src="<?php echo EVENT_URL;?>image/facebook-dark/facebook-dark.png" srcset="<?php echo EVENT_URL;?>image/facebook-dark/facebook-dark@2x.png 2x,<?php echo EVENT_URL;?>image/facebook-dark/facebook-dark@3x.png 3x" class="facebook-dark"></a>
                                            <a href="https://plus.google.com/share?url=<?php echo $url; ?>/event/<?php echo $query[0]->id; ?>" class="social"><img src="<?php echo EVENT_URL;?>image/gplus-dark/gplus-dark.png" srcset="<?php echo EVENT_URL;?>image/gplus-dark/gplus-dark@2x.png 2x,<?php echo EVENT_URL;?>image/gplus-dark/gplus-dark@3x.png 3x" class="gplus-dark"></a>
                                            <a href="https://www.xing.com/spi/shares/new?url=<?php echo $url; ?>/event/<?php echo $query[0]->id; ?>" class="social"><img src="<?php echo EVENT_URL;?>image/xing-dark/xing-dark.png" srcset="<?php echo EVENT_URL;?>image/xing-dark/xing-dark@2x.png 2x,<?php echo EVENT_URL;?>image/xing-dark/xing-dark@3x.png 3x" class="xing-dark"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   <?php } ?>
             </div>
            </div>
            <div class="a_footer">
                <p class="text-center"><a id="next" href="#"><strong>SHOW MORE</strong></a> </p>
            </div>
        </div>
    </div>
</div>
</div>