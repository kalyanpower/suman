<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script-->
<div class="cus-advertisement">
<?php
$files = $_FILES['attachment'];
$uploadimage = $_FILES['upload_image'];
$base_url = get_site_url();
$upload_overrides = array( 'test_form' => false );
$upload_overrides_1 = array( 'test_form' => false );
$filespath = ''; 
foreach($files['name'] as $key => $value) {
		    if ($files['name'][$key]) {
		    $uploadedfile = array(
		            'name'     => $files['name'][$key],
		            'type'     => $files['type'][$key],
		            'tmp_name' => $files['tmp_name'][$key],
		            'error'    => $files['error'][$key],
		            'size'     => $files['size'][$key]
		        );
			$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
			if ( $movefile && ! isset( $movefile['error'] ) ) {
			 	$filespath .= $movefile ['url']. '%%';
			} else {
			}
		}
}
foreach($uploadimage['name'] as $key_1 => $value_1) {
if ($uploadimage['name'][$key_1]){
		    $uploadedfile_1 = array(
		            'name'     => $uploadimage['name'][$key_1],
		            'type'     => $uploadimage['type'][$key_1],
		            'tmp_name' => $uploadimage['tmp_name'][$key_1],
		            'error'    => $uploadimage['error'][$key_1],
		            'size'     => $uploadimage['size'][$key_1]
		        );
			$movefile_1 = wp_handle_upload( $uploadedfile_1, $upload_overrides_1 );
			if ( $movefile_1 && ! isset( $movefile_1['error'] ) ) {
			 	$filespath_1 .= $movefile_1['url'];
			} 
			else {
			}
}
}

 $filespath = $_POST['key_client_name_hidden'].$filespath;
$filespath_1 = $_POST['key_uploadimage'].$filespath_1;

?>

<h2>Add Event</h2>
<?php if(is_user_logged_in()){ 
//if($_POST){
	if (isset($_POST['submit'])) {
		global $wpdb;
	    $user = wp_get_current_user();
		$userid = $user->ID;
		$event_name = $_POST['event_name'];
		 $event_date = $_POST['event_date'];
		$duration = $_POST['duration'];
		$event_time = $_POST['event_time'];
		$event_topic = $_POST['event_topic'];
		$address = $_POST['address'];
		$country = $_POST['country'];
		$address = $_POST['address'];
	    $event_mail = $_POST['event_mail'];
	    $doctitle = $_POST['doc_title'];
		$description = $_POST['description'];
		$table_name = $wpdb->prefix . "event_data";
		$query = $wpdb->insert($table_name, array('userid'=>$userid, 'eventname'=>$event_name, 'uploadimage'=>$filespath_1, 'date' => $event_date,'duration'=>$duration,'time' =>$event_time, 'topic' =>$event_topic,'address' =>$address,'email' =>$event_mail,'description'=>$description,'doctitle'=>$doctitle,'document'=>$filespath));
		 $quer = $wpdb->last_query;
	
		if($query)
		{
		$msg="<div id='deletesuccess'>Event Data have been created successfully </div>";
		echo '<h5 class="ad_success_msg">' . $msg.'</h5>';
		}
		else
		{
		echo 'There is some prolem found. Please try again later.';
	    }
	}
?>
		<form action="<?php echo site_url(); ?>/event_form/" method="post" enctype="multipart/form-data"  data-toggle="validator" role="form" id="addadvertise">
			
			<div class="col-md-12">
				<div class="form-group">
					<label for="name" class="control-label">Event Name:</label>
					<input type="text" class="form-control" name="event_name" id="eventname" required>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="certificates" class="control-label">Upload Image:</label>
					<input type="hidden" name="key_uploadimage" id="key_uploadimage" value="" class="regular-text key_uploadimage">
					<input type="file" class="form-control" name="upload_image[]" id="uploadimage" required>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="certificates" class="control-label">Date:</label>
					<input type="text" class="form-control" name="event_date" id="datepicker" required>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="price" class="control-label">Duration:</label>
					<input type="text" class="form-control" name="duration" id="duration" required>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="year_establish" class="control-label">Time:</label>
					<input type="time" class="form-control" name="event_time" id="event_time" required>
					<div class="help-block with-errors"></div>
				</div>
			</div> 
			<div class="col-md-6">
				<div class="form-group">
					<label for="financing" class="control-label">Topic:</label>
					<input type="text" class="form-control" name="event_topic" id="event_topic" required>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="legal_form" class="control-label">Address:</label>
					<input type="text" class="form-control" name="address" id="address" required>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="time_of_sale" class="control-label">Mail:</label>
					<input type="text" class="form-control" name="event_mail" id="event_mail" required>
					<div class="help-block with-errors"></div>
				</div>
			</div>	
			<div class="col-md-12">
				<div class="form-group">
					<label for="desc" class="control-label">Description:</label>
					<textarea class="form-control" name="description" id="description" required></textarea>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="desc" class="control-label">Document Title:</label>
					<input type="text" class="form-control" name="doc_title" id="doc_title" required>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group upload-image">
					<label for="uploadname" class="control-label">Document:</label>
					<input type="hidden" name="key_client_name_hidden" id="key_client_name" value="" class="regular-text key_client_name">
					<div class="input_key_client_name_wrap">
						<div><input type="file" name="attachment[]" class="regular-text" required></div>
					</div>
					<div class="add-attachment">
						<label for="key_client_name">Add Attachment</label>
						<button class="add_key_client_name_button">Add More </button>
					</div>
					<div class="help-block with-errors"></div>
				</div>
			</div>

			<div class="col-md-12">
				<div class="pay-now-btn text-center">
				</div>
			</div>
			
			<div class="cus-btn-submit">
				<div class="form-group payment_btn">
					<input type="submit" name="submit" value="Add Event">
				</div>
			</div>
		</form>
	</div>
<?php

}
else{
echo '<h2 class="loggedout_msg">You have to logged in to view this page</h2>';
}
?>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script>
	$( function() {
    $( "#datepicker" ).datepicker();
  } );
	 $(document).ready( function() {
        $('#deletesuccess').delay(2000).fadeOut();
      });
		jQuery(document).ready(function(){
   
			jQuery('a.delete').on('click',function(e){
				e.preventDefault();
				
				var repurl = jQuery(this).attr("href");				
				var replit = jQuery('input[name=key_client_name_hidden]').val().replace(repurl+'%%',"");
				jQuery('input[name=key_client_name_hidden]').val(replit);	
				imageID = jQuery(this).closest('.image1')[0].id;
				jQuery(this).closest('.image1')
					.fadeTo(300,0,function(){
						jQuery(this)
							.animate({width:0},200,function(){
								jQuery(this)
									.remove();
							});
					});
			});
			var max_fields      = 10; //maximum input boxes allowed
			var wrapper         = jQuery(".input_key_client_name_wrap"); //Fields wrapper
			var add_button      = jQuery(".add_key_client_name_button"); //Add button ID
			
			var x = 1; //initlal text box count
			jQuery(add_button).click(function(e){ //on add input button click
				e.preventDefault();
				if(x < max_fields){ //max input box allowed
					x++; //text box increment
					jQuery(wrapper).append('<div class="logo-add"> <input type="file" class="form-control" id="attachment" name="attachment[]" data-error="Please add attachment field" required><a href="#" class="remove_key_client_name" style="color:red;"><i class="fa fa-trash" aria-hidden="true"></i></a></div>'); //add input box
				}
			});
			
			jQuery(wrapper).on("click",".remove_key_client_name", function(e){ 
				e.preventDefault(); jQuery(this).parent('div').remove(); x--;
			})

			
			
		});
	</script>