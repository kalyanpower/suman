<?php
global $wpdb;
$url = $_SERVER['REQUEST_URI'];
$parts = explode("/", $url);
$event_id = $parts[3];
$table_name = $wpdb->prefix . "event_data"; 
$query= $wpdb->get_results("SELECT * from $table_name where id ='$event_id'");
$date=date_create($query[0]->date);
$date_f = date_format($date,"F d, Y");
//echo '<pre>';
//print_r($query);
$image = explode('%%', $query[0]->document);
//print_r($image);
?>
<div class="d-container">
    <div class="col-md-4">
        <div class="back-to-btn">
            <a href="#" class="btn btn-sm back-to-btn" ><img src="<?php echo EVENT_URL;?>icon/slick-previous.png">Back to Events</a>
        </div>
        <div class="image-slider1">
            <div>
			
                <img src="<?php echo $query[0]->uploadimage; ?>" >
            </div>
        </div>
        <div class="button-box">
            <h4>68 CHF</h4>
            <button CLASS="btn">DOWNLOAD</button>
            <div class="share-btns">
			<?php  $url = site_url();
			?>
                <a href="https://twitter.com/intent/tweet?url=<?=urlencode($url).'/event/'.$query[0]->id; ?>" class="twitter-btn"><img src="<?php echo EVENT_URL;?>image/twitter-m-dark/twitter-m-dark.png" srcset="<?php echo EVENT_URL;?>image/twitter-m-dark/twitter-m-dark@2x.png 2x, <?php echo EVENT_URL;?>image/twitter-m-dark/twitter-m-dark@3x.png 3x" class="twitter-m-dark"></a>
                <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>/event/<?php echo $query[0]->id; ?>" class="facebook-btn"><img src="<?php echo EVENT_URL;?>image/fb-m-dark/fb-m-dark.png" srcset="<?php echo EVENT_URL;?>image/fb-m-dark/fb-m-dark@2x.png 2x, <?php echo EVENT_URL;?>image/fb-m-dark/fb-m-dark@3x.png 3x" class="twitter-m-dark"></a>
                <a href="https://plus.google.com/share?url=<?php echo $url; ?>/event/<?php echo $query[0]->id;?>" class="gplus-btn"><img src="<?php echo EVENT_URL;?>image/gplus-m-dark/gplus-m-dark.png" srcset="<?php echo EVENT_URL;?>image/gplus-m-dark/gplus-m-dark@2x.png 2x, <?php echo EVENT_URL;?>image/gplus-m-dark/gplus-m-dark@3x.png 3x" class="twitter-m-dark"></a>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="d-body">
               <div class="margin-top-38px"></div>
            <h3><?php echo $query[0]->eventname; ?></h3>
            <div class="button-lst-grd">
                <a class="btn cat-hov" href="#"><img src="<?php echo EVENT_URL;?>image/category/category.png" srcset="<?php echo EVENT_URL;?>image/category/category@2x.png 2x,<?php echo EVENT_URL;?>image/category/category@3x.png 3x" class="category-hover"><?php echo $query[0]->topic; ?></a>
                <a class="btn loc-hov" href="#"><img src="<?php echo EVENT_URL;?>image/location/location.png" srcset="<?php echo EVENT_URL;?>image/location/location@2x.png 2x,<?php echo EVENT_URL;?>image/location/location@3x.png 3x" class="location-hover"><?php echo $query[0]->address; ?></a>
            </div>
           <div class="d-overview">
                    <div class="d-overview-block1">
                           <p><label>Date:</label><?php echo $date_f; ?></p>
                           <p><label>Time:</label><?php echo $query[0]->time; ?></p>
                           <p><label>Adress:</label><?php echo $query[0]->address; ?></p>
                    </div>
                    <div class="d-overview-block2">
                           <p><label>Duration:</label><?php echo $query[0]->duration; ?></p>
                           <p><label>Topic:</label><?php echo $query[0]->topic; ?></p>
                           <p><label>Mail:</label><?php echo $query[0]->mail; ?></p>
                    </div>
                </div>
                <div class="d-descr">
                    <h4>DESCRIPTION</h4>
                    <p><?php echo $query[0]->description; ?></p>
                </div>
                <div class="d-descr">
                    <h4>DOCUMENTS</h4>
                    <div class="doc-section">
					
                  <?php
					$result = array_filter($image);
						foreach($result as $img){ 
						?>
                        <div class="doc-item">
                            <img class="doc-icon" src="<?php echo EVENT_URL;?>image/filetype-pdf/filetype-pdf.png" srcset="<?php echo EVENT_URL;?>image/filetype-pdf/filetype-pdf@2x.png 2x, <?php echo EVENT_URL;?>image/filetype-pdf/filetype-pdf@3x.png 3x" class="filetype-pdf">
                            <h5><?php echo $query[0]->doctitle; ?></h5>
                            <a href="<?php echo $img; ?>" class="a-hover"><img src="<?php echo EVENT_URL;?>image/download-small/download-small.png" srcset="<?php echo EVENT_URL;?>image/download-small/download-small@2x.png 2x,<?php echo EVENT_URL;?>image/download-small/download-small@3x.png 3x" class="download-small"></a>
                        </div>
                    <?php } ?>
                    </div>
                </div>
           </div>
    </div>
</div>
<div class=" text-center">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2701.1598054796937!2d8.189090715934109!3d47.38931477917084!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47901676f188f58d%3A0xb36b254fd53377e6!2sNeuhofstrasse+36%2C+5600+Lenzburg%2C+Switzerland!5e0!3m2!1sen!2s!4v1518513345105" width="1092" height="300" frameborder="0" style="border:0; margin-bottom: -5px;" allowfullscreen></iframe>
</div>
<div class="text-center rel-pos">
    <button id="sd-button" class="btn hide-btn">^</button>

</div>