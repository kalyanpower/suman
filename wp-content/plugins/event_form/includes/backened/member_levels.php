<?php
include('scripts.php');
?>
<?php
global $wpdb;
$table_name = $wpdb->prefix . "membership_levels"; 
$query= $wpdb->get_results("SELECT * from $table_name");
?>
<h3>Membership Levels</h3><br>
<table id="reqsdata" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
			    <th>SrNo</th>
                <th>Membership Name</th>
                <th>Description</th>
                <th>Price</th>
		        <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
			    <th>SrNo</th>
                <th>Membership Name</th>
                <th>Description</th>
                <th>Price</th>
		        <th>Action</th>
            </tr>
        </tfoot>
<tbody>
<?php 
$count = 1;
foreach($query as $row){ 
echo '<tr>';
echo '<td>'. $count . '</td>';
echo '<td>'. $row->level_name; '</td>';
                echo '<td>'. $row->level_description . '</td>';
                echo '<td>'. $row->level_price . '</td>';
          echo '<td><a href="?page=edit_level&id='. $row->id . '">Edit</a> | <a href="?page=delete_level&id='. $row->id . '">Delete</a></td>';
		echo '</tr>';
		$count++;
} ?>
</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){
    $('#reqsdata').DataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>
