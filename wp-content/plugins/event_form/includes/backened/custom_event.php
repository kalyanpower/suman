<?php
include('scripts.php');
global $wpdb;
$table_name = $wpdb->prefix . "event_data"; 
$query = $wpdb->get_results("SELECT * from $table_name");
?>
<h3></h3><br>
<table id="reqsdata" class="display" cellspacing="0" width="100%">
<thead>
	<tr>
		<th>SrNo</th>
		<th>Event Name</th>
		<th>Topic</th>
		<th>Name</th>
		<th>Action</th>
	</tr>
</thead>
<tfoot>
	<tr>
	  <th>SrNo</th>
		<th>Event Name</th>
		<th>Topic</th>
		<th>Name</th>
		<th>Action</th>
	</tr>
</tfoot>
<tbody>
<?php 
$count = 1;
foreach($query as $row){
echo '<tr>';
echo '<td>'. $count . '</td>';
echo '<td>'. $row->eventname . '</td>';
echo '<td>'. $row->topic . '</td>';
echo '<td>'. $row->email . '</td>';
echo '<td><a href="admin.php?page=view_event&id=' . $row->id . '">View</a> | <a href="admin.php?page=delete_event&id=' . $row->id . '">Delete</a></td>';
echo '</tr>';
$count++;
} 
?>
</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){
    $('#reqsdata').DataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>
