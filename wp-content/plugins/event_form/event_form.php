<?php
/*
 Plugin Name: Event Form 
 Description: A plugin to add ads, contact, documents into website.
 Version: 1.2
 Author: Eweb
 License: GPL2
*/
define( 'EVENT_PATH', dirname( __FILE__ ) );
define( 'EVENT_URL', plugin_dir_url( __FILE__ ));

add_action('admin_menu', 'event_menu_pages');
function event_menu_pages(){
add_menu_page('Event', 'Event', 'manage_options', 'Event', 'custom_event','dashicons-media-interactive', 3 );
//add_submenu_page('Ads', 'Add Roles', 'Add Roles', 'manage_options', 'add_roles', 'add_roles');
add_submenu_page(NULL, 'View event', 'View event', 'manage_options', 'view_event', 'view_event');
add_submenu_page(NULL, 'Delete event', 'Delete event', 'manage_options', 'delete_event', 'delete_event');
}
function custom_event(){
	include_once(EVENT_PATH . "/includes/backened/custom_event.php");
}
function view_event(){
	include_once(EVENT_PATH . "/includes/backened/view_event.php");
}
function delete_event(){
	include_once(EVENT_PATH . "/includes/backened/delete_event.php");
}


function load_plugin_css_custom_1(){
  wp_enqueue_style( 'new_style' );
}
add_action('wp_enqueue_scripts', 'load_plugin_css_custom_1');

function event_init(){
global $wpdb;
add_shortcode( 'event_form', 'event_form_shortcode' );
add_shortcode( 'event_list', 'event_list_shortcode' );
add_shortcode( 'single_event', 'single_event_shortcode' );
wp_register_style( 'new_style', EVENT_URL . 'css/event_style.css', false, '1.0.0', 'all');
}
add_action('init', 'event_init');

function event_form_shortcode(){
ob_start();
include( EVENT_PATH . '/includes/frontend/event_form.php' );
$contents = ob_get_contents();
ob_end_clean();
return $contents;
}

function event_list_shortcode(){
ob_start();
include( EVENT_PATH . '/includes/frontend/event_list.php' );
$contents = ob_get_contents();
ob_end_clean();
return $contents;
}

function single_event_shortcode(){
ob_start();
include( EVENT_PATH . '/includes/frontend/single_event.php' );
$contents = ob_get_contents();
ob_end_clean();
return $contents;
}

add_action( 'template_redirect', 'redirect_to_specific_page' );
?>