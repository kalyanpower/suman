<?php
/*
 Plugin Name: Custom Membership
 Description: A plugin to add membership levels into signup form.
 Version: 1.2
 Author: Eweb
 License: GPL2
*/
define( 'MEMBER_PATH', dirname( __FILE__ ) );
define( 'MEMBER_URL', plugin_dir_url( __FILE__ ));
add_action('admin_menu', 'membership_menu_pages');
function membership_menu_pages(){
add_menu_page('Membership', 'Membership', 'manage_options', 'Membership', 'membership_sec','dashicons-building', 3 );
add_submenu_page('Membership', 'Add Level', 'Add Level', 'manage_options', 'add_level', 'add_level');
add_submenu_page(NULL, 'Edit Level', 'Edit Level', 'manage_options', 'edit_level', 'edit_level');
add_submenu_page(NULL, 'Delete level', 'Delete level', 'manage_options', 'delete_level', 'delete_level');
}
function membership_sec(){
	include_once(MEMBER_PATH . "/includes/backened/member_levels.php");
}
function add_level(){
	include_once(MEMBER_PATH . "/includes/backened/addlevel.php");
}
function edit_level(){
	include_once(MEMBER_PATH . "/includes/backened/edit_level.php");
}
function delete_level(){
	include_once(MEMBER_PATH . "/includes/backened/delete_level.php");
	
}

function load_plugin_css(){
wp_enqueue_style( 'style', MEMBER_URL . 'css/style.css' );
}
add_action( 'wp_enqueue_scripts', 'load_plugin_css' );
?>