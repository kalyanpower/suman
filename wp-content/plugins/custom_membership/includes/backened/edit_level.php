<?php
global $wpdb;
include_once("scripts.php");
$id=$_GET['id'];
$table_name = $wpdb->prefix . "membership_levels"; 
$query = $wpdb->get_results("SELECT * from $table_name where id='$id'");
$table_name_roles = $wpdb->prefix . "roles"; 
$query_role = $wpdb->get_results("SELECT * from $table_name_roles");
?>
<div class="add_package">
<h1 class="package_title">Edit Level</h1>
<form action="" id="edit_package" method="post" onsubmit="redirect();">
<div class="form-group">
    <label>Membership Name</label>
    <input type="text" class="form-control" name="level_name" value="<?php echo $query[0]->level_name; ?>">
</div>
<div class="form-group">
    <label>Select User Role</label>
    <?php
	echo '<select name="selectrole[]" multiple>';
	foreach($query_role as $role){ ?>
	<option value="<?php echo $role->roleid; ?>" <?php if(($role->roleid) == ($query[0]->role_assigned)){echo 'selected';} ?>><?php echo $role->rolename; ?></option>
	 <?php }	
	 echo '</select>';?>
</div>
<div class="form-group">
    <label>Description</label>
    <input type="text" class="form-control" name="level_description" value="<?php echo $query[0]->level_description; ?>">
</div>
<div class="form-group">
    <label>Price</label>
    <input type="text" class="form-control" name="level_price" value="<?php echo $query[0]->level_price; ?>">
</div>
<button type="submit" class="btn btn-primary" name="level_submit">Submit</button>
</form>
</div>
<?php 
if(isset($_POST['level_submit'])){
	$table_name = $wpdb->prefix . "membership_levels"; 
	$level_name=$_POST['level_name'];
	$level_role = implode(',', $_POST['selectrole']);
	$level_description=$_POST['level_description'];
	$level_price=$_POST['level_price'];
    $update = $wpdb->query("
    UPDATE $table_name
    SET level_name = '$level_name', role_assigned = '$level_role', level_description = '$level_description', level_price = '$level_price'
    WHERE id = '$id'");
} 
if($update){
	echo 'Update Successfully';
	echo '<script>window.location.href = "admin.php?page=Membership";</script>';
}
?>