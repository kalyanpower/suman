<?php global $wpdb; 
include_once('scripts.php');
$table_name_roles = $wpdb->prefix . "roles"; 
$query_role = $wpdb->get_results("SELECT * from $table_name_roles");
?>	
<div class="add_category">
<h1 class="category_title">Add Membership level</h1>
<form action="#" method="post">
  <div class="form-group">
    <label>Membership Name</label>
    <input type="text" class="form-control" name="level_name" required>
  </div>
  <div class="form-group">
    <label>Select User Role</label>
    <?php
	echo '<select name="selectrole">';
	foreach($query_role as $role){
	echo '<option value="' . $role->roleid . '">' . $role->rolename . '</option>'; 
	 }	
	 echo '</select>';?>
</div>
  <div class="form-group">
    <label style="width: 100%;">Description</label>
    <textarea name="level_description" style="width: 100%;height: 150px;"></textarea>
	</div>
  <div class="form-group">
    <label>Price</label>
    <input type="text" class="form-control" name="level_price">
  </div>
 
  <button type="submit" class="btn btn-primary" name="level_submit">Submit</button>
</form>
</div>
<?php
if(isset($_POST['level_submit'])){
$level_name=$_POST['level_name'];
$level_description=$_POST['level_description'];
$level_price=$_POST['level_price'];
$table_name = $wpdb->prefix . "membership_levels"; 
$query=$wpdb->insert($table_name, array(
    'id' => '',
	'level_name' => $level_name,
	'level_description' => $level_description,
	'level_price' => $level_price
));
if($query)
{
	echo '<p class="success_message">Membership Level added</p>';
	echo '<script>window.location.href = "admin.php?page=Membership";</script>';
}
}


?>