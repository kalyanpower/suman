<?php
/*
 Plugin Name: Custom User Roles
 Description: A plugin to add user roles into signup form.
 Version: 1.2
 Author: Eweb
 License: GPL2
*/
define( 'ROLE_PATH', dirname( __FILE__ ) );
define( 'ROLE_URL', plugin_dir_url( __FILE__ ));
add_action('admin_menu', 'roles_menu_pages');
function roles_menu_pages(){
add_menu_page('User Roles', 'User Roles', 'manage_options', 'User Roles', 'user_roles','dashicons-building', 3 );
add_submenu_page('User Roles', 'Add Roles', 'Add Roles', 'manage_options', 'add_roles', 'add_roles');
add_submenu_page(NULL, 'Edit Role', 'Edit Role', 'manage_options', 'edit_role', 'edit_role');
add_submenu_page(NULL, 'Delete Role', 'Delete Role', 'manage_options', 'delete_role', 'delete_role');
}
function user_roles(){
	include_once(ROLE_PATH . "/includes/backened/user_roles.php");
}
function add_roles(){
	include_once(ROLE_PATH . "/includes/backened/addroles.php");
}
function edit_role(){
	include_once(ROLE_PATH . "/includes/backened/edit_role.php");
}
function delete_role(){
	include_once(ROLE_PATH . "/includes/backened/delete_role.php");
}
function load_plugin_css_n(){
wp_enqueue_style( 'style', $plugin_url . 'css/style.css' );
}
add_action('wp_enqueue_scripts', 'load_plugin_css_n');
?>