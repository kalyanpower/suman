<?php
global $wpdb;
include_once("scripts.php");
$id=$_GET['roleid'];
$table_name = $wpdb->prefix . "roles"; 
$query = $wpdb->get_results("SELECT * from $table_name where roleid='$id'");
?>
<div class="add_role">
<h1 class="role_title">Edit Role</h1>
<form action="#" method="post">
  <div class="form-group">
  <label>Role Name</label>
  <input type="text" class="form-control" value="<?php echo $query[0]->rolename; ?>" name="role_name">
  </div>
  <div class="form-group">
  <label>Member Price</label>
  <input type="text" class="form-control" name="member_price" value="<?php echo $query[0]->member_price; ?>" required>
  </div>
  <div class="form-group">
  <?php 
  $permissions = explode(',', $query[0]->permissions);
  ?>
  <label style="width: 100%;">Set Permissions</label>
  <input type="checkbox" name="permissions[]" value="1" <?php foreach($permissions as $per){
	if( $per==1){
		echo "checked";
	}

  }?>>All ads published and active<br>
  <input type="checkbox" name="permissions[]" value="2" <?php foreach($permissions as $per){
	if( $per==2){
		echo "checked";
	}
	  
  }?>>Ads published no more active but not yet sold<br>
  <input type="checkbox" name="permissions[]" value="3" <?php foreach($permissions as $per){
	if( $per==3){
		echo "checked";
	}
	  
  }?>>Ads never active but placed by us<br>
  <input type="checkbox" name="permissions[]" value="4" <?php foreach($permissions as $per){
	if( $per==4){
		echo "checked";
	}
	  
  }?>>Has a smart search function to pull list out of a database of all companies existing in Switzerland.<br>
  <input type="checkbox" name="permissions[]" value="5" <?php foreach($permissions as $per){
	if( $per==5){
		echo "checked";
	}
	  
  }?>>Listed with a very short profile as expert.<br>
  <input type="checkbox" name="permissions[]" value="6" <?php foreach($permissions as $per){
	if( $per==6){
		echo "checked";
	}
	  
  }?>>Listed with a profile as expert.<br>
  <input type="checkbox" name="permissions[]" value="7" <?php foreach($permissions as $per){
	if( $per==7){
		echo "checked";
	}
	  
  }?>>Listed with a detailed profile as expert<br>
   <?php //} ?>
  </div>
  <button type="submit" class="btn btn-primary" name="role_submit">Submit</button>
</form>
</div>

<?php 
if(isset($_POST['role_submit'])){
	$table_name = $wpdb->prefix . "roles"; 
	$role_name=$_POST['role_name'];
	$mem_price = $_POST['member_price'];
	$permissions = implode(',', $_POST['permissions']);
    $update = $wpdb->query("
    UPDATE $table_name
    SET rolename = '$role_name', permissions = '$permissions', member_price = '$mem_price'
    WHERE roleid = '$id'");
} 
if($update){
	echo 'Update Successfully';
	echo '<script>window.location.href = "admin.php?page=User+Roles";</script>';
}
?>