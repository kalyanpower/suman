<?php
include('scripts.php');
global $wpdb;
$table_name = $wpdb->prefix . "roles"; 
$query= $wpdb->get_results("SELECT * from $table_name");
?>
<h3></h3><br>
<table id="reqsdata" class="display" cellspacing="0" width="100%">
<thead>
	<tr>
		<th>SrNo</th>
		<th>Role Name</th>
		<th>Action</th>
	</tr>
</thead>
<tfoot>
	<tr>
	  <th>SrNo</th>
	  <th>Role Name</th>
	  <th>Action</th>
	</tr>
</tfoot>
<tbody>
<?php 
$count = 1;
foreach($query as $row){
echo '<tr>';
echo '<td>'. $count . '</td>';
echo '<td>'. $row->rolename . '</td>';
echo '<td><a href="?page=edit_role&roleid='. $row->roleid . '">Edit</a> | <a href="?page=delete_role&roleid='. $row->roleid . '">Delete</a></td>';
echo '</tr>';
$count++;
} 
?>
</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){
    $('#reqsdata').DataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>
