<?php global $wpdb; 
include_once('scripts.php');
?>	
<div class="add_role">
<h1 class="role_title">Add New Role</h1>
<form action="#" method="post">
  <div class="form-group">
  <label>Role Name</label>
  <input type="text" class="form-control" name="role_name" required>
  </div>
  <div class="form-group">
  <label>Member Price</label>
  <input type="text" class="form-control" name="member_price" required>
  </div>
  <div class="form-group">
  <label style="width: 100%;">Set Permissions</label>
  <input type="checkbox" name="permissions[]" value="1">All ads published and active<br>
  <input type="checkbox" name="permissions[]" value="2">Ads published no more active but not yet sold<br>
  <input type="checkbox" name="permissions[]" value="3">Ads never active but placed by us<br>
  <input type="checkbox" name="permissions[]" value="4">Has a smart search function to pull list out of a database of all companies existing in Switzerland.<br>
  <input type="checkbox" name="permissions[]" value="5">Listed with a very short profile as expert.<br>
  <input type="checkbox" name="permissions[]" value="6">Listed with a profile as expert.<br>
  <input type="checkbox" name="permissions[]" value="7">Listed with a detailed profile as expert<br>
  </div>
  <button type="submit" class="btn btn-primary" name="role_submit">Submit</button>
</form>
</div>
<?php
if(isset($_POST['role_submit'])){
$role_name = $_POST['role_name'];
$mem_price = $_POST['member_price'];
$permissions = implode(',', $_POST['permissions']);
$table_name = $wpdb->prefix . "roles"; 
	$query = $wpdb->insert($table_name, array(
		'roleid' => '',
		'rolename' => $role_name,
		'member_price' => $mem_price,
		'permissions' => $permissions
	));
	if($query)
	{
		echo '<p class="success_message">New Role added</p>';
		echo '<script>window.location.href = "admin.php?page=User+Roles";</script>';
	}
}
?>