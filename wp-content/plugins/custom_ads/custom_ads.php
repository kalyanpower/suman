<?php
/*
 Plugin Name: Custom Ads 
 Description: A plugin to add ads, contact, documents into website.
 Version: 1.2
 Author: Eweb
 License: GPL2
*/
define( 'ADS_PATH', dirname( __FILE__ ) );
define( 'ADS_URL', plugin_dir_url( __FILE__ ));
add_action('admin_menu', 'ad_menu_pages');
function ad_menu_pages(){
add_menu_page('Ads', 'Ads', 'manage_options', 'Ads', 'custom_ads','dashicons-media-interactive', 3 );
//add_submenu_page('Ads', 'Add Roles', 'Add Roles', 'manage_options', 'add_roles', 'add_roles');
add_submenu_page(NULL, 'View Ad', 'View Ad', 'manage_options', 'view_ads', 'view_ads');
add_submenu_page(NULL, 'Delete Ad', 'Delete Ad', 'manage_options', 'delete_ad', 'delete_ad');
}



function load_plugin_css_custom(){
wp_enqueue_style( 'style', ADS_URL . 'css/style.css' );
}
add_action('wp_enqueue_scripts', 'load_plugin_css_custom');

function ads_init(){
global $wpdb;
add_shortcode( 'addads', 'addAds_shortcode' );
add_shortcode( 'singleads', 'singleads_shortcode' );
add_shortcode( 'myads', 'myads_shortcode' );
}
add_action('init', 'ads_init');

function addAds_shortcode(){
ob_start();
include( ADS_PATH . '/includes/frontend/add_ads.php' );
$contents = ob_get_contents();
ob_end_clean();
return $contents;
}
function singleads_shortcode(){
ob_start();
include( ADS_PATH . '/includes/frontend/single_ad.php' );
$contents = ob_get_contents();
ob_end_clean();
return $contents;
}
function myads_shortcode(){
ob_start();
include( ADS_PATH . '/includes/frontend/my_ads.php' );
$contents = ob_get_contents();
ob_end_clean();
return $contents;
}
function custom_ads(){
	include_once(ADS_PATH . "/includes/backened/custom_ads.php");
}
/*function add_roles(){
	include_once(ROLE_PATH . "/includes/backened/addroles.php");
}*/
function view_ads(){
	include_once(ADS_PATH . "/includes/backened/view_ads.php");
}
function delete_ad(){
	include_once(ADS_PATH . "/includes/backened/delete_ad.php");
}

add_action( 'template_redirect', 'redirect_to_specific_page' );


?>