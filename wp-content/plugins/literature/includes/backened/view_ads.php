<?php
global $wpdb;
include_once("scripts.php");
$id=$_GET['id'];
$table_name = $wpdb->prefix . "literature"; 
$query = $wpdb->get_results("SELECT * from $table_name where id='$id'");
$aimage = explode("%%",$query[0]->upload_image);
$user_info = get_userdata($query[0]->userid);

?>
<style>.edit_ad table {

}
td.adlabel {
    font-weight: bold;
}
.edit_ad  td{
	width:50%;
	border: 1px solid black;
	}
</style>
<div class="container">
<div class="edit_ad">
<h1 class="role_title">Literature</h1>
<table class="table table-striped">
<thead>
      <tr>
        <th></th>
        <th></th>
      </tr>
    </thead>
<tbody>
<tr><td class="adlabel">Id</td><td><?php echo $query[0]->id; ?></td></tr>
<tr><td class="adlabel">Username</td><td><?php echo $user_info->user_login ; ?></td></tr>
<tr><td class="adlabel">Property Name</td><td><?php echo $query[0]->property_name; ?></td></tr>
<tr><td class="adlabel">Images</td><td><?php foreach($aimage as $ii){  if($ii != ''){echo '<img src="' . $ii . '" style="width:100px;height:100px;margin:20px;">'; } } ?></td></tr>
<tr><td class="adlabel">Selling Price</td><td><?php echo $query[0]->selling_price; ?></td></tr>
<tr><td class="adlabel">Industry</td><td><?php echo $query[0]->industry; ?></td></tr>
<tr><td class="adlabel">Contact</td><td><?php echo $query[0]->contact; ?></td></tr>
<tr><td class="adlabel">Licenses</td><td><?php echo $query[0]->licenses; ?></td></tr>
<tr><td class="adlabel">Country</td><td><?php echo $query[0]->country; ?></td></tr>
<tr><td class="adlabel">Address</td><td><?php echo $query[0]->address; ?></td></tr>
<tr><td class="adlabel">Certificates</td><td><?php echo $query[0]->certificates; ?></td></tr>
<tr><td class="adlabel">Price</td><td><?php echo $query[0]->price; ?></td></tr>
<tr><td class="adlabel">Year Establish</td><td><?php echo $query[0]->year_establish; ?></td></tr>
<tr><td class="adlabel">Financing</td><td><?php echo $query[0]->financing; ?></td></tr>
<tr><td class="adlabel">Legal Form</td><td><?php echo $query[0]->legal_form; ?></td></tr>
<tr><td class="adlabel">Time of Sale</td><td><?php echo $query[0]->time_of_sale; ?></td></tr>
<tr><td class="adlabel">Employees</td><td><?php echo $query[0]->employees; ?></td></tr>
<tr><td class="adlabel">Property</td><td><?php echo $query[0]->property; ?></td></tr>
<tr><td class="adlabel">Turnover</td><td><?php echo $query[0]->turnover; ?></td></tr>
<tr><td class="adlabel">Description</td><td><?php echo $query[0]->description; ?></td></tr>
<tr><td class="adlabel">Management</td><td><?php echo $query[0]->management; ?></td></tr>
</tbody>
</table>
</div>

