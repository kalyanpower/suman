<?php
include('scripts.php');
global $wpdb;
$table_name = $wpdb->prefix . "literature"; 
$query = $wpdb->get_results("SELECT * from $table_name");
//echo "<pre>"; print_r($query); die;

?>
<h3></h3><br>
<table id="reqsdata" class="display" cellspacing="0" width="100%">
<thead>
	<tr>
		<th>SrNo</th>
		<th>Property Name</th>
		<th>Selling Price</th>
		<th>Action</th>
	</tr>
</thead>
<tfoot>
	<tr>
	  <th>SrNo</th>
	  <th>Property Name</th>
	  <th>Selling Price</th>
	  <th>Action</th>
	</tr>
</tfoot>
<tbody>
<?php 
$count = 1;
foreach($query as $row){
echo '<tr>';
echo '<td>'. $count . '</td>';
echo '<td>'. $row->property_name . '</td>';
echo '<td>'. $row->selling_price . '</td>';
echo '<td><a href="admin.php?page=view_lite&id=' . $row->id . '">View</a> | <a href="admin.php?page=delete_lite&id=' . $row->id . '">Delete</a></td>';
echo '</tr>';
$count++;
} 
?>
</tbody>
</table>
<script type="text/javascript">
$.fn.dataTable.ext.errMode = 'none';
$(document).ready(function(){
    $('#reqsdata').DataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>
