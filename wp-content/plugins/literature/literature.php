<?php
/*
 Plugin Name: Literature
 Description: A plugin to add ads, contact, documents into website.
 Version: 1.2
 Author: Eweb
 License: GPL2
*/
define( 'LITE_PATH', dirname( __FILE__ ) );
define( 'LITE_URL', plugin_dir_url( __FILE__ ));
add_action('admin_menu', 'add_menu_pages');
function add_menu_pages(){
add_menu_page('Literature', 'Literature', 'manage_options', 'Lite', 'custom_lite','dashicons-media-interactive', 3 );

add_submenu_page(NULL, 'View Ad', 'View Ad', 'manage_options', 'view_lite', 'view_lite');
add_submenu_page(NULL, 'Delete Ad', 'Delete Ad', 'manage_options', 'delete_lite', 'delete_lite');
}
function load_plugin_css_custom_lite(){
wp_enqueue_style( 'style', LITE_URL . 'css/style.css' );
}
add_action('wp_enqueue_scripts', 'load_plugin_css_custom_lite');

function literature_init(){
global $wpdb;
add_shortcode( 'litrature', 'add_lite_shortcode' );
add_shortcode( 'singliterature', 'single_lite_shortcode' );
add_shortcode( 'mylitrture', 'my_lite_shortcode' );
}
add_action('init', 'literature_init');

function add_lite_shortcode(){
ob_start();
include( LITE_PATH . '/includes/frontend/add_literature.php' );
$contents = ob_get_contents();
ob_end_clean();
return $contents;
}
function single_lite_shortcode(){
ob_start();
include( LITE_PATH . '/includes/frontend/single_ad.php' );
$contents = ob_get_contents();
ob_end_clean();
return $contents;
}
function my_lite_shortcode(){
ob_start();
include( LITE_PATH . '/includes/frontend/my_literature.php' );
$contents = ob_get_contents();
ob_end_clean();
return $contents;
}
function custom_lite(){
	include_once(LITE_PATH . "/includes/backened/custom_ads.php");
}
/*function add_roles(){
	include_once(ROLE_PATH . "/includes/backened/addroles.php");
}*/
function view_lite(){
	include_once(LITE_PATH . "/includes/backened/view_ads.php");
}
function delete_lite(){
	include_once(LITE_PATH . "/includes/backened/delete_ad.php");
}

add_action( 'template_redirect', 'redirect_to_specific_page' );


?>