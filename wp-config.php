<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'companymarekt');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'admin123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0B,?T*{`V(X(}wB!ouO|GwI/mbUvVV 5_u;BlZ7Oi-,d_9?a#; 5$cBimL,uo*5,');
define('SECURE_AUTH_KEY',  '[3%Q)%ipXoIbywo{C@guh31>1ejQ~w}bLP kG?%0K,2#kGqNB`<0f^3M(-BORFg0');
define('LOGGED_IN_KEY',    '*/dKI7:DB*yGm0gV|{qk1wffQuF@;!49Z$D);;pi|j^5ow|CB D/|HRz/s[uv`=c');
define('NONCE_KEY',        'sYQmTz|P*x@ YB RZQJKGxX}@wBU4&gi+-J 6PV+7=`4D]/ Y0>mDmzfn4h7,lJ@');
define('AUTH_SALT',        'pm2uTl2C@37t<u{J~e!Uxu.Ht1&rK }2ZHz2Oq3YUl?N1!.!4o zc(9B+M;y7c0)');
define('SECURE_AUTH_SALT', '/o}8Kii{tR,;[Q}^@CldqBO>+ 33:Amg!_Xm:B4f?X}AV=~}B4JeoBwP^2a`v0~`');
define('LOGGED_IN_SALT',   'd]@)9Gxe6oRPE}Hh yOEe]-@+oT*{3a3t,yu_,+K4tba<,*J< &@]n5@1,c]4Bfd');
define('NONCE_SALT',       'CY%_8P9f.]%-R&e hEtY^B+afwTev`~;u[2F. wUX_HL~1.hu6tofM~?f+_z=i# ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
